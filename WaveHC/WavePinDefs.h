/**
 * \file
 * Pin definitions
 * MODIFIED 10/29/2010 by Luke Spicer
 * DAC pins mapped to 38, 39, 40, 41 on arduino mega!
 */
#include <ArduinoPins.h>
#ifndef WavePinDefs_h
#define WavePinDefs_h

//SPI pin definitions

/** SPI slave select pin.*/
#define SS   SS_PIN

/** SPI master output, slave input pin. */
#define MOSI MOSI_PIN

/** SPI master input, slave output pin. */
#define MISO MISO_PIN

/** SPI serial clock pin. */
#define SCK  SCK_PIN

//------------------------------------------------------------------------------
// DAC pin definitions

// LDAC may be connected to ground to save a pin
/** Set USE_MCP_DAC_LDAC to 0 if LDAC is grounded. */
#define USE_MCP_DAC_LDAC 1

// ORIGINAL use arduino pins 2, 3, 4, 5 for DAC
// MODIFIED by Luke Spicer 10/29/2010
//		Use arduino pins 38, 39, 40, 42 for DAC

// ORIGINAL pin 2 is DAC chip select
// MODIFIED pin 38 is DAC chip select
/** Data direction register for DAC chip select. */
#define MCP_DAC_CS_DDR  PIN38_DDRREG
/** Port register for DAC chip select. */
#define MCP_DAC_CS_PORT PIN38_PORTREG
/** Port bit number for DAC chip select. */
#define MCP_DAC_CS_BIT  PIN38_BITNUM

// ORIGINAL pin 3 is DAC serial clock
// MODIFIED pin 39 is DAC serial clock
/** Data direction register for DAC clock. */
#define MCP_DAC_SCK_DDR  PIN39_DDRREG
/** Port register for DAC clock. */
#define MCP_DAC_SCK_PORT PIN39_PORTREG
/** Port bit number for DAC clock. */
#define MCP_DAC_SCK_BIT  PIN39_BITNUM

// ORIGINAL pin 4 is DAC serial data in
// MODIFIED pin 40 is DAC serial data in
/** Data direction register for DAC serial in. */
#define MCP_DAC_SDI_DDR  PIN40_DDRREG
/** Port register for DAC clock. */
#define MCP_DAC_SDI_PORT PIN40_PORTREG
/** Port bit number for DAC clock. */
#define MCP_DAC_SDI_BIT  PIN40_BITNUM

// ORIGINAL pin 5 is LDAC if used
// MODIFIED pin 41 is LDAC if used
#if USE_MCP_DAC_LDAC
/** Data direction register for Latch DAC Input. */
#define MCP_DAC_LDAC_DDR  PIN41_DDRREG
/** Port register for Latch DAC Input. */
#define MCP_DAC_LDAC_PORT PIN41_PORTREG
/** Port bit number for Latch DAC Input. */
#define MCP_DAC_LDAC_BIT  PIN41_BITNUM
#endif // USE_MCP_DAC_LDAC

#endif // WavePinDefs_h