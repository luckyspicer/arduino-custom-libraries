#include "WirelessComm.h"
// Contains basic functionality comments. Will update comments later.

// Constructor: Begins serial and defines the sensor variable names.
WirelessComm::WirelessComm(int serialPortNumber){
  _serialPortNumber = serialPortNumber;
  beginSerial();
}

// Begins serial communications at the BAUD rate specified in WirelessComm.h
void WirelessComm::beginSerial(){
  switch(_serialPortNumber) {
    case 1:
      Serial1.begin(BAUD);
      break; 
    case 2:
      Serial2.begin(BAUD);
      break;
    case 3:
      Serial3.begin(BAUD);
      break;
    default:
      Serial.begin(BAUD);
      break;      
  }
}

// Writes to the serial port defined when the object was created
void WirelessComm::send(String packet){
	switch(_serialPortNumber) {
    case 1:
      Serial1.print(packet);
      break; 
    case 2:
      Serial2.print(packet);
      break;
    case 3:
      Serial3.print(packet);
      break;
    default:
      Serial.print(packet);
      break;      
  }
}

// Sets up the variable name order (this should be the order that variables are passed
// to transmitData's float* vars)
String WirelessComm::_gpsVars[] = {"LATD","LATM","LATS","LOND","LONM","LONS","ALT","SPD","NSAT"};
String WirelessComm::_barVars[] = {"P","A","T"};
String WirelessComm::_humVars[] = {"H"};
String WirelessComm::_sirVars[] = {"S"};
String WirelessComm::_uvVars[]  = {"R"};
String WirelessComm::_rgbVars[] = {"R","G","B"};
String WirelessComm::_imuVars[] = {"XA","YA","ZA","XR","YR"};

// Formats the packet and sends it
void WirelessComm::transmitData(unsigned long timeStamp, String sensorName, float* vars){
	// Template strings
	send("<{\"t\":");
	send(String(timeStamp));
	send(",\"s\":\"");
	send(sensorName);
	send("\",\"d\":{");
	
	// Format the data array
	sendVars(sensorName, vars);
	
	// Add the suffix
	send("}}>\n");
}

// Formats the variables for the given sensorName
void WirelessComm::sendVars(String sensorName, float* vars){
	String dataTemplate = "\"%var%\":%val%";
	
	int numVars;
	String* varNames;
	
	if(sensorName == "GPS"){
		numVars = GPSS;
		varNames = _gpsVars;
	} else if(sensorName == "BAR"){
		numVars = BAR;
		varNames = _barVars;
	} else if(sensorName == "HUM"){
		numVars = HUM;
		varNames = _humVars;
	} else if(sensorName == "SIR"){
		numVars = SIR;
		varNames = _sirVars;
	} else if(sensorName == "UV"){
		numVars = UV;
		varNames = _uvVars;
	} else if(sensorName == "RGB"){
		numVars = RGB;
		varNames = _rgbVars;
	} else if(sensorName == "IMU"){
		numVars = IMU;
		varNames = _imuVars;
	} else {
		return;
	}
	
	// Sends all key:value pairs for the arrays pointed to by varNames and vars
	for(int i = 0; i < numVars; i++){
		send("\"");
		send(varNames[i]);
		send("\":");
		
		// ***KEY***
		// dtostrf(FLOAT,WIDTH,PRECSISION,BUFFER);
		// converts a float to a character array, which can become a String object
		char buff[15];
		send(String(dtostrf(vars[i],9,5,buff)).trim());
		
		if(i != (numVars - 1))
			send(",");
	}
}