#include <WirelessComm.h>
#define SERIALNUM 0

WirelessComm myWireless(SERIALNUM);
float gpsVars[9] = {84.023, 35.031, 22, 88.023, 30.031, 29, 102, 12, 7};
float barVars[3] = {128.023, 124, 23.4};
float humVars[1] = {55.2};
float sirVars[1] = {60.01232};
float uvVars[1]  = {11.19988};
float rgbVars[3] = {10, 20, 30};
float imuVars[5] = {0.2, 0.7, 20, 0.1, 0.3};

String gps = "GPS";
String bar = "BAR";
String hum = "HUM";
String sir = "SIR";
String uv  = "UV";
String rgb = "RGB";
String imu = "IMU";

void setup() 
{
  myWireless.beginSerial();
}

void loop()
{
  myWireless.transmitData(millis(), gps, gpsVars);
  myWireless.transmitData(millis(), bar, barVars);
  myWireless.transmitData(millis(), hum, humVars);
  myWireless.transmitData(millis(), sir, sirVars);
  myWireless.transmitData(millis(), uv, uvVars);
  myWireless.transmitData(millis(), rgb, rgbVars);
  myWireless.transmitData(millis(), imu, imuVars);
}
