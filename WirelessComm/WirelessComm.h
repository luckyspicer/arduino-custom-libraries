/*
Valid sensors definition:

GPS     = { -- GPS Module
        LATD = true, -- Degrees
        LATM = true, -- Minutes
        LATS = true, -- Seconds
        LOND = true, -- Degrees
        LONM = true, -- Minutes
        LONS = true, -- Seconds
        ALT  = true, -- Altitude
        SPD  = true, -- Speed
        NSAT = true  -- Number of satellite locks
},
BAR     = { -- Barometer
        P    = true, -- Barometeric Pressure
        A    = true, -- Altitude
        T    = true  -- Temperature
},
HUM     = { -- Humidity Sensor
        H    = true -- Relative humidity
},
SIR     = { -- Solar irradiance sensor
        S    = true -- Solar irradiance
},
UV      = { -- Ultraviolet radiation
        R    = true -- UV radiation
},
RGB     = { -- RGB color sensor
        R    = true, -- Red
        G    = true, -- Green
        B    = true  -- Blue
},
IMU     = { -- IMU Sensor
        xa       = true,
        ya       = true,
        za       = true,
        yr       = true,
        xr       = true
}
*/

#ifndef wireless_com_h
#define wireless_com_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define BAUD 9600

// Define number of variables per sensor
#define GPSS 9
#define BAR 3
#define HUM 1
#define SIR 1
#define UV  1
#define RGB 3
#define IMU 5

class WirelessComm
{
  // PUBLIC Methods
  public:
    WirelessComm(int serialPortNumber);
    void transmitData(unsigned long timeStamp, String sensorName, float* vars);
	void beginSerial(void);
	
  // PRIVATE Methods
  private:
    void sendVars(String sensorName, float* vars);
    void send(String packet);
  // PRIVATE Variables
	int _serialPortNumber;
	
	static String _gpsVars[];
    static String _barVars[];
    static String _humVars[];
    static String _sirVars[];
    static String _uvVars[];
    static String _rgbVars[];
    static String _imuVars[];
};

#endif