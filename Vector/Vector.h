/* 
 * File:   Vector.h
 * Author: Adrian
 *
 * Created on January 27, 2012, 6:03 PM
 */

#ifndef VECTOR_H
#define	VECTOR_H

class Vector {
    
public:
    Vector(float angle);
    Vector(float x, float y);
    
    float getAngle();
    float getMagnitude();
    float getX();
    float getY();
    void setAngle(float angle);
    void setMagnitude(float magnitude);
    void setXY(float x, float y);
    
    void rotate(float theta);
    float cmpAngle(Vector* cmpVector);
    
private:
    float angle;
    float magnitude;
    float x;
    float y;
    void update(int type);
};

#endif	/* VECTOR_H */

