#include <Vector.h>

void setup() {

  Serial.begin(9600);
  Serial.print("Vector Library Test\n");
  Serial.println("Initializing Vector v1 with x=-4 and y=4:");
  Vector v1 = Vector(-4,4);
  Serial.print("|v1|=");
  Serial.println(v1.getMagnitude());
  Serial.print("<v1=");
  Serial.println(v1.getAngle());
  Serial.print("x component of v1=");
  Serial.println(v1.getX());
  Serial.print("x component of v1=");
  Serial.println(v1.getY());
  Serial.println("Rotating the angle of v1 by 30 degrees:");
  v1.rotate(30);
  Serial.print("New <v1=");
  Serial.println(v1.getAngle());
  
  Serial.println("Initializing Vector v2 an angle of 15 degrees:");
  Vector v2 = Vector(15);
  Serial.print("<v2=");
  Serial.println(v2.getAngle());
  Serial.print("Comparing the angles of v1 and v2");
  Serial.print("<v2-<v1=");
  Serial.println(v1.cmpAngle(&v2));
  Serial.println("Entering turning decision loop");
}

void loop() {

}
