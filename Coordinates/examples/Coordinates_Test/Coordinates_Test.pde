#include <Coordinates.h>
#include <Vector.h>

void setup() {
  Serial.begin(9600);
  Serial.println("Coordinate System Map Test:");
  Serial.println("Creating coordinate system with origin at (38 12'45.918\"),(-85 45'36.457\")");
  Coordinates c = Coordinates(38, 12,45.918,-85,45,36.457);
  Serial.print("Square Ratio=");
  Serial.println(c.getSqRatio(),5);
  Serial.print("Origin x seconds=");
  Serial.println(c.getX0());
  Serial.print("Origin y seconds=");
  Serial.println(c.getY0());
  Serial.println("Mapping point (12'30\"),(45'36.457\") to coordinate system:");
  c.map_point(12,30,45,30);
  Serial.print("Mapped x=");
  Serial.println(c.getX());
  Serial.print("Mapped y=");
  Serial.println(c.getY());
  Serial.println("Mapping COG with an angle of 271 degrees to coordinate system:");
  Serial.print("Mapped angle=");
  Serial.println(c.map_COG(271));
}

void loop() {
}
