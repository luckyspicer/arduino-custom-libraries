//
//  point.cpp
//  UofLUSLI
//
//  Created by Nicholas Searcy on 1/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "Coordinates.h"

#define PI 3.14159265

Coordinates::Coordinates(){
    
}

Coordinates::Coordinates(int lat_degrees, int lat_minutes, float lat_seconds, int lon_degrees, int lon_minutes, float lon_seconds){
    lat_seconds += 60*lat_minutes;
    lon_seconds += 60*lon_minutes;
    y0 = lat_seconds;
    x0 = lon_seconds;
    sqRatio = cos((lat_degrees+(lat_minutes/60))*PI/180);
}

void Coordinates::map_point(int lat_minutes, float lat_seconds, int lon_minutes, float lon_seconds){
    lat_seconds += 60*lat_minutes;
    lon_seconds += 60*lon_minutes;
    x = lon_seconds - x0;
    y = lat_seconds - y0;
    
    if (x < -1800){
        x += 3600;
    }
    else if (x > 1800){
        x -= 3600;
    }
    if (y < -1800) {
        y += 3600;
    }
    else if (y > 1800){
        y -= 3600;
    }
	x = x/sqRatio;
}

float Coordinates::map_COG(float angle){
	Vector COG = Vector(90-angle);
	float x1 = COG.getX()/sqRatio;
	float y1 = COG.getY();
	COG.setXY(x1,y1);
	return COG.getAngle();
	
}

float Coordinates::getX(){
    return x;
}

float Coordinates::getY(){
    return y;
}

float Coordinates::getX0() {
    return x0;
}

float Coordinates::getY0() {
    return y0;
}

float Coordinates::getSqRatio()
{
    return sqRatio;
}