//
//  point.h
//  UofLUSLI
//
//  Created by Nicholas Searcy on 1/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef COORDINATES_H
#define COORDINATES_H

#include <Vector.h>
#include <math.h>

class Coordinates {
    
public:
    Coordinates();
    Coordinates(int lat_degrees, int lat_minutes, float lat_seconds, int lon_degrees, int lon_minutes, float lon_seconds);
    void map_point(int lat_minutes, float lat_seconds, int lon_minutes, float lon_seconds);
    float map_COG(float angle);
    float getX();
    float getY();
    float getX0();
    float getY0();
    float getSqRatio();
    
private:
    float x;
    float y;
    float x0;
    float y0;
    float sqRatio;
};

#endif
