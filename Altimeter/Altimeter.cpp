//
// University of Louisville USLI, 2011-2012
//
// Filename		:	Altimeter.cpp
// Written by	:	Lucas Spicer
// Date			:	14 December 2011
// Description 	:	C++ file which contains function definitions for the custom
//					Altimeter class. The constructor allows the device to be attached
//					to any of the 4 hardware serial ports of the arduino mega, or the default
//					single Serial port for the Uno. The function, get_altitude()
//					simply returns the latest altitude of the device in ft above ground level
//					of whatever configuration the logging altimeter is set up for. The serial
//					boad rate is assumed to be 9600 as described in the PerfectFlite altimeter
//					user manuals, but should be adjusted for whatever particular device is being used.
//					A private function to the library, wait_for_data() pauses execution until the altimeter
//					as sent at least 10 bytes of altitude data, or the system times out. If the system
//					times out, it may indicate a impropperly configured, broken, disconnected 
//					or damaged altimeter. An altitude of -999 indicates the device timed out 
//

#include "Altimeter.h"

//****** PUBLIC//******//
//
// Simple Altimeter constructor Altimeter() for use with hardware serial ports
//
// Input:	Serial Port Number
// Return:	N.A.
//
// Description: Creates an Altimeter interface object attached to the specified serial port
//
Altimeter::Altimeter(int serial_port_number) {
  _serial_port_number = serial_port_number;
  if(_serial_port_number == 0) {		// if _serial_port_number = 0, attach to Serial
	Serial.begin(9600);					// default buad rate for PerfectFlite altimeters 9600
	Serial.setTimeout(100);				// Timeout prevents altimeter device malfunction from
										// locking up the entire system
  }
  else if(_serial_port_number == 1) {	// if _serial_port_number = 1, attach to Serial1
	Serial1.begin(9600);				// default buad rate for PerfectFlite altimeters 9600
	Serial1.setTimeout(100);			// Timeout prevents altimeter device malfunction from
										// locking up the entire system
  }
  else if(_serial_port_number == 2) {	// if _serial_port_number = 2, attach to Serial1
	Serial2.begin(9600);				// default buad rate for PerfectFlite altimeters 9600
	Serial2.setTimeout(100);			// Timeout prevents altimeter device malfunction from
										// locking up the entire system
  }						
  else if(_serial_port_number == 3) {	// if _serial_port_number = 2, attach to Serial1
	Serial3.begin(9600);				// default buad rate for PerfectFlite altimeters 9600
	Serial3.setTimeout(100);			// Timeout prevents altimeter device malfunction from
										// locking up the entire system
  }
  else {								// else (for other numbers) attach to default Serial
	Serial3.begin(9600);				// default buad rate for PerfectFlite altimeters 9600
	Serial3.setTimeout(100);			// Timeout prevents altimeter device malfunction from
										// locking up the entire system
  }
}

//
// Function: get_altitude()
// 
// Input: 	void
// Return:	int altitude in ft above ground level (or setting of attached
//			altimeter). If Altimeter becomes disconnected then it the is fucntion will return
//			an altitude of -999 ft, which indicates a timeout of the serial port
//
int Altimeter::get_altitude() {
  int altitude;							// local variable to store altitude in
  if (_serial_port_number == 0) {		// if _serial_port_number = 0, user Serial
	altitude = wait_for_data();			// if wait_for_data returns -999, then altimeter timed out
    while(Serial.available() >= 10) {	// Read until we have the most recent altitude
      altitude = Serial.parseInt();		// user parseInt() to read altidue from serial
    }
  }
  else if (_serial_port_number == 1) {	// if _serial_port_number = 1, user Serial1
	altitude = wait_for_data();			// if wait_for_data returns -999, then altimeter timed out
    while(Serial1.available() >= 10) {	// Wait until there are at least 10 bytes to read
      altitude = Serial1.parseInt();	// Read until we have the most recent altitude
    }									// user parseInt() to read altidue from Serial
  }
  else if (_serial_port_number == 2) {	// if _serial_port_number = 2, user Serial2
	altitude = wait_for_data();			// if wait_for_data returns -999, then altimeter timed out
    while(Serial2.available() >= 10) {	// Read until we have the most recent altitude
      altitude = Serial2.parseInt();	// user parseInt() to read altidue from Serial2
    }
  }
  else if (_serial_port_number == 3) {	// if _serial_port_number = 3, user Serial3
	altitude = wait_for_data();			// if wait_for_data returns -999, then altimeter timed out
    while(Serial3.available() >= 10) {	// Read until we have the most recent altitude
      altitude = Serial3.parseInt();	// user parseInt() to read altidue from Serial3
    }
  }
  else	{								// if other use Serial
	altitude = wait_for_data();			// if wait_for_data returns -999, then altimeter timed out
    while(Serial.available() >= 10) {	// Read until we have the most recent altitude
      altitude = Serial.parseInt();		// user parseInt() to read altidue from serial
    }
  }
  return altitude;						// return the latest altitude in ft above ground level
}

//
// Function: wait_for_data()
// 
// Input: 	void
// Return:	int (1 if we have data to parse for altitude value, -999 if timeout)
//
int Altimeter::wait_for_data(void) {
  unsigned long timeout = millis();			// timeout time is initialized to the current time
  int isvalid = -999;						// if the device times out -999 (indicating timeout) is returned
  while(millis() - timeout < 100) {			// Altimeter has 100mSec to read value before timeout occurs
	  if (_serial_port_number == 0) {		// if _serial_port_number = 0, user Serial
		if(Serial.available() > 10) {		// Wait until there are at least 10 bytes to read
			isvalid = 1;					// If Serial available >= 10 device is good so return
			return isvalid;
		}
	  }
	  else if (_serial_port_number == 1) {	// if _serial_port_number = 1, user Serial1
		if(Serial1.available() > 10) {		// Wait until there are at least 10 bytes to read
			isvalid = 1;					// If Serial available >= 10 device is good so return
			return isvalid;
		}
	  }
	  else if (_serial_port_number == 2) {	// if _serial_port_number = 2, user Serial2
		if(Serial2.available() > 10) {		// Wait until there are at least 10 bytes to read
			isvalid = 1;					// If Serial available >= 10 device is good so return
			return isvalid;
		}
	  }
	  else if (_serial_port_number == 3) {	// if _serial_port_number = 3, user Serial3
		if(Serial3.available() > 10) {		// Wait until there are at least 10 bytes to read
			isvalid = 1;					// If Serial available >= 10 device is good so return
			return isvalid;
		}
	  }
	  else	{								// if other use Serial
		if(Serial.available() > 10) {		// Wait until there are at least 10 bytes to read
			isvalid = 1;					// If Serial available >= 10 device is good so return
			return isvalid;
		}
	  }
  }
  return isvalid;							// If we get to this statement, then timeout occurred
}