// Author:		Luke Spicer
// Date:		July 11th, 2012
// Purpose:		Wheel Encoder Class Implementation
// Description:	

#include "WheelEncoder.h"

// Global variables and interrupt service routines (ISR) for the possible encoders
// The unnamed namespace prohibits access to the otherwise global
//	 variables from any other source files
// Each encoder will be conditionally compiled based on the #define USE_WHEEL_ENCODER
namespace
{
	// Constants
	static const uint32_t MAX_ENCODER_PULSE_PERIOD_US = 1000001; //62500;
	static const uint32_t MAX_ENCODER_PULSE_PERIOD_MS = 30;	//62;
#if defined(USE_WHEEL_ENCODER0)
	// Constants for Encoder #0
	// Change the MULTIPLIER if the encoder uses a different resolution (1x, 2x and
	// 4x are all common quadrature encoder resolutions).
	// The MULTIPLIER0_POW2 is the power of 2 that would give MULTIPLIER, for
	// example 2^0 = 1, 2^1 = 2, 2^2 = 4, 2^3 = 8, etc.
	static const uint8_t MULTIPLIER0 = 16;
	static const uint8_t MULTIPLIER0_POW2 = 4;
	
	// Global Variables for Encoder #0
	// Count and Direction Variables
	volatile int32_t encoderCount0 = 0;
	volatile uint8_t encoderDir0;
	int16_t encoderDirPin0;
	int16_t encoderClkPin0;
	// Period and Frequency Variables
	volatile uint8_t isMoved0 = 0;
	volatile uint32_t encoderPeriodBuf0[MULTIPLIER0] = {0};
	volatile uint8_t encoderPeriodBufIdx0 = 0;
	volatile uint32_t lastEncoderPulseTime0 = 0;
	volatile uint32_t lastEncoderPulseMillis0 = 0;
	volatile uint8_t lastClkLvl0;
#endif

#if defined(USE_WHEEL_ENCODER1)
	// Constants for Encoder #1
	// Change the MULTIPLIER if the encoder uses a different resolution (1x, 2x and
	// 4x are all common quadrature encoder resolutions).
	// The MULTIPLIER0_POW2 is the power of 2 that would give MULTIPLIER, for
	// example 2^0 = 1, 2^1 = 2, 2^2 = 4, 2^3 = 8, etc.
	static const uint8_t MULTIPLIER1 = 16;
	static const uint8_t MULTIPLIER1_POW2 = 4;
	
	// Global Variables for Encoder #1
	// Count and Direction Variables
	volatile int32_t encoderCount1 = 0;
	volatile uint8_t encoderDir1;
	int16_t encoderDirPin1;
	int16_t encoderClkPin1;
	// Period and Frequency Variables
	volatile uint8_t isMoved1 = 0;
	volatile uint32_t encoderPeriodBuf1[MULTIPLIER1] = {0};
	volatile uint8_t encoderPeriodBufIdx1 = 0;
	volatile uint32_t lastEncoderPulseTime1 = 0;
	volatile uint32_t lastEncoderPulseMillis1 = 0;
	volatile uint8_t lastClkLvl1;
#endif
	
	// ISR for all Encoders
	// This ISR modifies only volatile global variables specific to itself
	void EncoderISR(void)
	{
		// Get the Current time since reset in micro-seconds
		uint32_t tempMicroTime = micros();
		uint32_t tempMilliTime = millis();
		uint16_t tempClk;

#if defined(USE_WHEEL_ENCODER0)
		tempClk = digitalRead(encoderClkPin0);
		if ((tempClk != lastClkLvl0) || (isMoved0 == 0))
		{
			// Position: count up or count down based on the direction
			// Clockwise when looking at the attached wheel/motor is positive
			if (digitalRead(encoderDirPin0) == 1)
			{
				++encoderCount0;
				encoderDir0 = 1;
			}
			else
			{
				--encoderCount0;
				encoderDir0 = 0;
			}

			// Period: Measure the time since the last encoder pulse
			//         Store in the running average ring buffer
			// Use isMove to indicate that the encoder has moved since reset
			isMoved0 = 1;
			encoderPeriodBuf0[encoderPeriodBufIdx0] = ElapsedUS(lastEncoderPulseTime0, tempMicroTime);
			encoderPeriodBufIdx0 = (encoderPeriodBufIdx0 + 1) % MULTIPLIER0;
			lastEncoderPulseTime0 = tempMicroTime;
			lastEncoderPulseMillis0 = tempMilliTime;
			lastClkLvl0 = tempClk;
		}
#endif
#if defined(USE_WHEEL_ENCODER1)
		tempClk = digitalRead(encoderClkPin1);
		if ((tempClk != lastClkLvl1) || (isMoved1 == 0))
		{
			// Position: count up or count down based on the direction
			// Clockwise when looking at the attached wheel/motor is positive
			if (digitalRead(encoderDirPin1) == 1)
			{
				++encoderCount1;
				encoderDir1 = 1;
			}
			else
			{
				--encoderCount1;
				encoderDir1 = 0;
			}

			// Period: Measure the time since the last encoder pulse
			//         Store in the running average ring buffer
			// Use isMove to indicate that the encoder has moved since reset
			isMoved1 = 1;
			encoderPeriodBuf1[encoderPeriodBufIdx1] = ElapsedUS(lastEncoderPulseTime1, tempMicroTime);
			encoderPeriodBufIdx1 = (encoderPeriodBufIdx1 + 1) % MULTIPLIER1;
			lastEncoderPulseTime1 = tempMicroTime;
			lastEncoderPulseMillis1 = tempMilliTime;
			lastClkLvl1 = tempClk;
		}
#endif
	}

#if defined(USE_WHEEL_ENCODER2)
	// Constants for Encoder #2
	// Change the MULTIPLIER if the encoder uses a different resolution (1x, 2x and
	// 4x are all common quadrature encoder resolutions).
	// The MULTIPLIER_POW2 is the power of 2 that would give MULTIPLIER, for
	// example 2^0 = 1, 2^1 = 2, 2^2 = 4, 2^3 = 8, etc.
	static const uint8_t MULTIPLIER2 = 4;
	static const uint8_t MULTIPLIER2_POW2 = 2;
	
	// Global Variables for Encoder #2
	// Count and Direction Variables
	volatile int32_t encoderCount2 = 0;
	volatile uint8_t encoderDir2;
	int16_t encoderDirPin2;
	// Period and Frequency Variables
	volatile uint8_t isMoved2 = 0;
	volatile uint32_t encoderPeriodBuf2[MULTIPLIER2];
	volatile uint8_t encoderPeriodBufIdx2 = 0;
	volatile uint32_t lastEncoderPulseTime2 = 0;
	
	// ISR for Encoder #2
	// This ISR modifies only volatile global variables specific to itself
	void EncoderISR2(void)
	{
		// Get the Current time since reset in micro-seconds
		uint32_t tempMicroTime = micros();

		// Position: count up or count down based on the direction
		// Clockwise when looking at the attached wheel/motor is positive
		if (digitalRead(encoderDirPin2) == 1)
		{
			++encoderCount2;
			encoderDir2 = 1;
		}
		else
		{
			--encoderCount2;
			encoderDir2 = 0;
		}

		// Period: Measure the time since the last encoder pulse
		//         Store in the running average ring buffer
		// Use isMove to indicate that the encoder has moved since reset
		isMoved2 = 1;
		encoderPeriodBuf2[encoderPeriodBufIdx2] = tempMicroTime - lastEncoderPulseTime2;
		encoderPeriodBufIdx2 = (encoderPeriodBufIdx2 + 1) % MULTIPLIER2;
		lastEncoderPulseTime2 = tempMicroTime;
	}
#endif

#if defined(USE_WHEEL_ENCODER3)
	// Constants for Encoder #3
	// Change the MULTIPLIER if the encoder uses a different resolution (1x, 2x and
	// 4x are all common quadrature encoder resolutions).
	// The MULTIPLIER_POW2 is the power of 2 that would give MULTIPLIER, for
	// example 2^0 = 1, 2^1 = 2, 2^2 = 4, 2^3 = 8, etc.
	static const uint8_t MULTIPLIER3 = 4;
	static const uint8_t MULTIPLIER3_POW2 = 2;
	
	// Global Variables for Encoder #3
	// Count and Direction Variables
	volatile int32_t encoderCount3 = 0;
	volatile uint8_t encoderDir3;
	int16_t encoderDirPin3;
	// Period and Frequency Variables
	volatile uint8_t isMoved3 = 0;
	volatile uint32_t encoderPeriodBuf3[MULTIPLIER3];
	volatile uint8_t encoderPeriodBufIdx3 = 0;
	volatile uint32_t lastEncoderPulseTime3 = 0;
	
	// ISR for Encoder #3
	// This ISR modifies only volatile global variables specific to itself
	void EncoderISR3(void)
	{
		// Get the Current time since reset in micro-seconds
		uint32_t tempMicroTime = micros();

		// Position: count up or count down based on the direction
		// Clockwise when looking at the attached wheel/motor is positive
		if (digitalRead(encoderDirPin3) == 1)
		{
			++encoderCount3;
			encoderDir3 = 1;
		}
		else
		{
			--encoderCount3;
			encoderDir3 = 0;
		}

		// Period: Measure the time since the last encoder pulse
		//         Store in the running average ring buffer
		// Use isMove to indicate that the encoder has moved since reset
		isMoved3 = 1;
		encoderPeriodBuf3[encoderPeriodBufIdx3] = tempMicroTime - lastEncoderPulseTime3;
		encoderPeriodBufIdx3 = (encoderPeriodBufIdx3 + 1) % MULTIPLIER3;
		lastEncoderPulseTime3 = tempMicroTime;
	}
#endif

#if defined(USE_WHEEL_ENCODER4)
	// Constants for Encoder #4
	// Change the MULTIPLIER if the encoder uses a different resolution (1x, 2x and
	// 4x are all common quadrature encoder resolutions).
	// The MULTIPLIER_POW2 is the power of 2 that would give MULTIPLIER, for
	// example 2^0 = 1, 2^1 = 2, 2^2 = 4, 2^3 = 8, etc.
	static const uint8_t MULTIPLIER4 = 4;
	static const uint8_t MULTIPLIER4_POW2 = 2;
	
	// Global Variables for Encoder #4
	// Count and Direction Variables
	volatile int32_t encoderCount4 = 0;
	volatile uint8_t encoderDir4;
	int16_t encoderDirPin4;
	// Period and Frequency Variables
	volatile uint8_t isMoved4 = 0;
	volatile uint32_t encoderPeriodBuf4[MULTIPLIER4];
	volatile uint8_t encoderPeriodBufIdx4 = 0;
	volatile uint32_t lastEncoderPulseTime4 = 0;
	
	// ISR for Encoder #4
	// This ISR modifies only volatile global variables specific to itself
	void EncoderISR4(void)
	{
		// Get the Current time since reset in micro-seconds
		uint32_t tempMicroTime = micros();

		// Position: count up or count down based on the direction
		// Clockwise when looking at the attached wheel/motor is positive
		if (digitalRead(encoderDirPin4) == 1)
		{
			++encoderCount4;
			encoderDir4 = 1;
		}
		else
		{
			--encoderCount4;
			encoderDir4 = 0;
		}

		// Period: Measure the time since the last encoder pulse
		//         Store in the running average ring buffer
		// Use isMove to indicate that the encoder has moved since reset
		isMoved4 = 1;
		encoderPeriodBuf4[encoderPeriodBufIdx4] = tempMicroTime - lastEncoderPulseTime4;
		encoderPeriodBufIdx4 = (encoderPeriodBufIdx4 + 1) % MULTIPLIER4;
		lastEncoderPulseTime4 = tempMicroTime;
	}
#endif

#if defined(USE_WHEEL_ENCODER5)
	// Constants for Encoder #5
	// Change the MULTIPLIER if the encoder uses a different resolution (1x, 2x and
	// 4x are all common quadrature encoder resolutions).
	// The MULTIPLIER_POW2 is the power of 2 that would give MULTIPLIER, for
	// example 2^0 = 1, 2^1 = 2, 2^2 = 4, 2^3 = 8, etc.
	static const uint8_t MULTIPLIER5 = 4;
	static const uint8_t MULTIPLIER5_POW2 = 2;
	
	// Global Variables for Encoder #1
	// Count and Direction Variables
	volatile int32_t encoderCount5 = 0;
	volatile uint8_t encoderDir5;
	int16_t encoderDirPin5;
	// Period and Frequency Variables
	volatile uint8_t isMoved5 = 0;
	volatile uint32_t encoderPeriodBuf5[MULTIPLIER5];
	volatile uint8_t encoderPeriodBufIdx5 = 0;
	volatile uint32_t lastEncoderPulseTime5 = 0;
	
	// ISR for Encoder #5
	// This ISR modifies only volatile global variables specific to itself
	void EncoderISR5(void)
	{
		// Get the Current time since reset in micro-seconds
		uint32_t tempMicroTime = micros();

		// Position: count up or count down based on the direction
		// Clockwise when looking at the attached wheel/motor is positive
		if (digitalRead(encoderDirPin5) == 1)
		{
			++encoderCount5;
			encoderDir5 = 1;
		}
		else
		{
			--encoderCount5;
			encoderDir5 = 0;
		}

		// Period: Measure the time since the last encoder pulse
		//         Store in the running average ring buffer
		// Use isMove to indicate that the encoder has moved since reset
		isMoved5 = 1;
		encoderPeriodBuf5[encoderPeriodBufIdx5] = tempMicroTime - lastEncoderPulseTime5;
		encoderPeriodBufIdx5 = (encoderPeriodBufIdx5 + 1) % MULTIPLIER5;
		lastEncoderPulseTime5 = tempMicroTime;
	}
#endif
}

// Encoder Class Definitions
// Public Methods
// Details of the method interfaces is available in WheelEncoder.h
void WheelEncoder::Connect(InterruptConstants::PinChangeInterruptNumber_t pci_num, const int16_t dir_pin)
{
	Reset();
	*_encoderDirPin = dir_pin;
	*_encoderClkPin = InterruptConstants::GetPinChangeInterruptPin(pci_num);
	_inter_num = pci_num;
	// Use the Arduino function to attach the given encoder ISR to the hardware interrupt
	InterruptConstants::AttachPinChangeInterrupt(pci_num, _encoderISR);
}

void WheelEncoder::Disconnect(void)
{
	InterruptConstants::DetachPinChangeInterrupt(_inter_num);
}

int32_t WheelEncoder::GetCount(void)
{
	return (*_encoderCount);
}

uint8_t WheelEncoder::GetDirection(void)
{
	return (*_encoderDir);
}

void WheelEncoder::Reset(void)
{
	*_encoderCount = 0;
	*_isMoved = 0;
	for (uint8_t i = 0; i < _multiplier; ++i)
	{
		_encoderPeriodBuf[i] = MAX_ENCODER_PULSE_PERIOD_US;
	}
}

uint32_t WheelEncoder::GetFrequency(void)
{
	// If isMoved == 0 that means the encoder has not moved since reset
	if(*_isMoved == 0) {
		return (0);
	}
	else
	{
		// In order to compensate for deceleration we need to modify the period buffer
		// based on how long it has been since we last registered a pulse.
		uint32_t tempMissedPeriods = (ElapsedMS(*_lastPulseTime, millis()) / MAX_ENCODER_PULSE_PERIOD_MS);
		*_lastPulseTime += (tempMissedPeriods * MAX_ENCODER_PULSE_PERIOD_MS);
		for(uint8_t i = 0; (i < tempMissedPeriods) && (i < _multiplier); ++i)
		{
			_encoderPeriodBuf[*_encoderPeriodBufIdx] = MAX_ENCODER_PULSE_PERIOD_US;
			*_encoderPeriodBufIdx = (*_encoderPeriodBufIdx + 1) % _multiplier;
		}

		// Calculate the average period and thereby the frequency of the
		// encoder CLK pulses
		// The average is calculated based the multiplier used
		// If the multiplier is 2, the last two pulse periods are averaged,
		// If the multiplier is 4, the last four pulse periods are averaged, etc.
		// The periods are in integer numbers of microseconds
		uint32_t tempAvgPeriod = 0;
		for(uint8_t i = 0; i < _multiplier; ++i)
		{
		  tempAvgPeriod += _encoderPeriodBuf[i];
		}
		tempAvgPeriod = (tempAvgPeriod >> _multiplier_pow2);

		// The frequency in Hz is found by dividing the number of microseconds in 1 sec
		// By the average period in microseconds
		return (US_PER_SEC / tempAvgPeriod);
	}
}

// Encoder Pre-instantiations
#if defined(USE_WHEEL_ENCODER0)
WheelEncoder WheelEncoder0(&encoderCount0, &encoderDir0, &encoderDirPin0, &encoderClkPin0, encoderPeriodBuf0, &encoderPeriodBufIdx0, &isMoved0, &lastEncoderPulseTime0, &lastEncoderPulseMillis0, &lastClkLvl0, MULTIPLIER0, MULTIPLIER0_POW2, EncoderISR);
#endif

#if defined(USE_WHEEL_ENCODER1)
WheelEncoder WheelEncoder1(&encoderCount1, &encoderDir1, &encoderDirPin1, &encoderClkPin1, encoderPeriodBuf1, &encoderPeriodBufIdx1, &isMoved1, &lastEncoderPulseTime1, &lastEncoderPulseMillis1, &lastClkLvl1, MULTIPLIER1, MULTIPLIER1_POW2, EncoderISR);
#endif

#if defined(USE_WHEEL_ENCODER2)
WheelEncoder WheelEncoder2(&encoderCount2, &encoderDir2, &encoderDirPin2, encoderPeriodBuf2, &encoderPeriodBufIdx2, &isMoved2, &lastEncoderPulseTime2, MULTIPLIER2, MULTIPLIER2_POW2, EncoderISR2);
#endif

#if defined(USE_WHEEL_ENCODER3)
WheelEncoder WheelEncoder3(&encoderCount3, &encoderDir3, &encoderDirPin3, encoderPeriodBuf3, &encoderPeriodBufIdx3, &isMoved3, &lastEncoderPulseTime3, MULTIPLIER3, MULTIPLIER3_POW2, EncoderISR3);
#endif

#if defined(USE_WHEEL_ENCODER4)
WheelEncoder WheelEncoder4(&encoderCount4, &encoderDir4, &encoderDirPin4, encoderPeriodBuf4, &encoderPeriodBufIdx4, &isMoved4, &lastEncoderPulseTime4, MULTIPLIER4, MULTIPLIER4_POW2, EncoderISR4);
#endif

#if defined(USE_WHEEL_ENCODER5)
WheelEncoder WheelEncoder5(&encoderCount5, &encoderDir5, &encoderDirPin5, encoderPeriodBuf5, &encoderPeriodBufIdx5, &isMoved5, &lastEncoderPulseTime5, MULTIPLIER5, MULTIPLIER5_POW2, EncoderISR5);
#endif
