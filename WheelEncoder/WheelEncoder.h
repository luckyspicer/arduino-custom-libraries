// Author:		Luke Spicer
// Date:		July 11th, 2012
// Purpose:		Wheel Encoder Class Declaration (Header)
// Description:	Declares a Wheel Encoder class for use with various
//				wheel encoders such as the WheelWatcher WW-12.
//				The class assumes the encoder provides a CLK output
//				which toggles, pulses low or otherwise indicates
//				when either the ChA or ChB quadrature channels
//				experiences a transition. The class also assumes
//				the encoder provides a direction output which has
//				a value of logic HIGH when the encoder rotates
//				clockwise looking down on the shaft and a value of
//				logic LOW when the encoder rotates anti-clockwise.
//				The Wheel Encoder class requires two hardware inputs.
//				The first input connects the encoder CLK to a hardware
//				interrupt line. The second connects the encoder
//				direction output to a GPIO. The class provides
//				methods to return the number of ticks the encoder
//				has rotated, the direction of rotation, the frequency
//				of ticks (in Hz) as well as to reset the tick count
//				and connect or disconnect the encoder.
//
//				Hardware Encoder (or hardware encoder) refers to the physical
//				encoder hardware (sensors, circuitry, etc.)
//
//				WheelEncoder refers to this software class or an object
//				instance of this class
//				

#ifndef wheel_encoder_h
#define wheel_encoder_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include <inttypes.h>
#include "InterruptConstants.h"
#include "StandardConstants.h"
#include "GPIO.h"

// Uncomment any WheelEncoder that you wish to use. Realize that any defined
// Wheel Encoder will use system resources like program space and RAM
#define USE_WHEEL_ENCODER0
#define USE_WHEEL_ENCODER1
//#define USE_WHEEL_ENCODER2
//#define USE_WHEEL_ENCODER3
//#define USE_WHEEL_ENCODER4
//#define USE_WHEEL_ENCODER5

class WheelEncoder {
	public:
		///
		// Constructor
		// Connects the static global variables for the selected WheelEncoder ISR
		// to the private members of the class using pointers and an initialization list
		// The WheelEncoder class constructor is not intended to be allocated at runtime,
		// rather the encoders are pre-initialized at compile time. This constructor
		// should not be called except for pre-initialization in the implementation file.
		WheelEncoder(volatile int32_t* encoderCount, volatile uint8_t* encoderDir,
						int16_t* encoderDirPin, int16_t* encoderClkPin,
						volatile uint32_t* encoderPeriodBuf,
						volatile uint8_t* encoderPeriodBufIdx, volatile uint8_t* isMoved,
						volatile uint32_t* lastEncoderPulseTime, volatile uint32_t* lastPulseTime,
						volatile uint8_t* lastClkLvl, const uint8_t multiplier,
						const uint8_t multiplier_pow2,
						void (*encoderISR)(void)) :
						_encoderDirPin(encoderDirPin),
						_encoderClkPin(encoderClkPin),
						_encoderCount(encoderCount),
						_encoderDir(encoderDir),
						_speedReady(0),
						_encoderPeriodBuf(encoderPeriodBuf),
						_encoderPeriodBufIdx(encoderPeriodBufIdx),
						_isMoved(isMoved),
						_lastEncoderPulseTime(lastEncoderPulseTime),
						_lastPulseTime(lastPulseTime),
						_lastClkLvl(lastClkLvl),
						_multiplier(multiplier),
						_multiplier_pow2(multiplier_pow2),
						_encoderISR(encoderISR),
						_inter_num(InterruptConstants::UNUSED_PICINT) {}
		///
		// Connect
		// Return: 		void
		// Parameters:	pin change interrupt number (interrupt numbers defined in Interrupt_types.h)
		//				direction pin (Arduino GPIO pin number connected to the encoder direction output)
		// Description: Connects the hardware encoder to the Arduino interrupt hardware and GPIO
		// Result:		Hardware encoder CLK pulses after call to Connect will be counted
		void Connect(InterruptConstants::PinChangeInterruptNumber_t pci_num, const int16_t dir_pin);
		
		///
		// Disconnect
		// Return: 		void
		// Parameters:	void
		// Description: Disconnects the hardware encoder from the Arduino interrupt hardware
		// Result:		New CLK pulses from the hardware encoder will not be counted
		void Disconnect(void);
		
		///
		// GetCount
		// Return: 		signed 32 bit integer (int32_t) encoder count (the number of CLK
		//				pulses from the hardware encoder since board reset or call to
		//				<WheelEncoder>.Reset()
		// Parameters:	void
		// Description: Returns the number of CLK pulses seen from the encoder since
		//				board reset or call to <WheelEncoder>.Reset()
		int32_t GetCount(void);
		
		///
		// GetDirection
		// Return: 		unsigned 8 bit integer (uint8_t) encoder direction
		// Parameters:	void
		// Description: Returns the current direction of the encoder. A value of 1
		//				indicates the code wheel is rotating clockwise (as seen
		//				when looking down at the rotating shaft).
		//				A value of 0 indicates the code wheel is rotating anti-clockwise.
		uint8_t GetDirection(void);
		
		///
		// GetFrequency
		// Return: 		unsigned 32 bit integer (uint32_t) encoder pulse frequency
		// Parameters:	void
		// Description: Returns the current frequency of the CLK signal coming from
		//				the hardware encoder (in Hz), where the period is the time
		//				between each CLK event.
		uint32_t GetFrequency(void);
		
		///
		// Reset
		// Return: 		void
		// Parameters:	void
		// Description: Resets the WheelEncoder (returns object to initial state)
		// Result:		The number of CLK pulses seen is set to zero
		//				The current output frequency is set to zero
		void Reset(void);
	
	private:
		// These pointers point to global variables the Encoder ISR modifies
		// Because the interrupt can modify these variables they are volatile
		int16_t* _encoderDirPin;
		int16_t* _encoderClkPin;
		volatile int32_t* _encoderCount;
		volatile uint8_t* _encoderDir;
		volatile int8_t* _speedReady;
		volatile uint32_t* _encoderPeriodBuf;
		volatile uint8_t* _encoderPeriodBufIdx;
		volatile uint8_t* _isMoved;
		volatile uint32_t* _lastEncoderPulseTime;
		volatile uint32_t* _lastPulseTime;
		volatile uint8_t* _lastClkLvl;
		const uint8_t _multiplier;
		const uint8_t _multiplier_pow2;
		// This function pointer points to the Encoder ISR
		void (*_encoderISR)(void);
		// These values are private member variables that the ISR does not modify
		InterruptConstants::PinChangeInterruptNumber_t _inter_num;
};	// End Class WheelEncoder definition

// These pre-instantiated WheelEncoders are instantiated in WheelEncoder.cpp
#if defined(USE_WHEEL_ENCODER0)
extern WheelEncoder WheelEncoder0;
#endif

#if defined(USE_WHEEL_ENCODER1)
extern WheelEncoder WheelEncoder1;
#endif

#if defined(USE_WHEEL_ENCODER2)
extern WheelEncoder WheelEncoder2;
#endif

#if defined(USE_WHEEL_ENCODER3)
extern WheelEncoder WheelEncoder3;
#endif

#if defined(USE_WHEEL_ENCODER4)
extern WheelEncoder WheelEncoder4;
#endif

#if defined(USE_WHEEL_ENCODER5)
extern WheelEncoder WheelEncoder5;
#endif

#endif
