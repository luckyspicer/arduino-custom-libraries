#######################################
# Syntax Coloring Map For WheelEncoder
#######################################

#######################################
# Datatypes (KEYWORD1)
#######################################

WheelEncoder	KEYWORD1
WheelEncoder0	KEYWORD1
WheelEncoder1	KEYWORD1
WheelEncoder2	KEYWORD1
WheelEncoder3	KEYWORD1
WheelEncoder4	KEYWORD1
WheelEncoder5	KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################
Connect	KEYWORD2
Disconnect	KEYWORD2
GetCount	KEYWORD2
GetDirection	KEYWORD2
GetDirection	KEYWORD2
GetFrequency	KEYWORD2
Reset	KEYWORD2

#######################################
# Constants (LITERAL1)
#######################################
SERIAL0	LITERAL1
SERIAL1	LITERAL1
SERIAL2	LITERAL1
SERIAL3	LITERAL1