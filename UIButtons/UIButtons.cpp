// Author:		Luke Spicer
// Date:		February 9th, 2013
// Purpose:		UIButtons Class Implementation
// Description:	Implementation of the UIButtons class including the UIButtons ISR

#include "UIButtons.h"
//
// Local Declarations
//
 
static uint32_t UIButtonsPressTimer = 0;
///
// UIButtonsISR
// Return: 		void
// Parameters:	void
// Description: When the interrupt attached to the UIButtons is triggerd
//				this routine simply saves the current time using millis().
//				This is used by the WhichUIButtonPressed() method to determine a press
static void UIButtonsISR(void)
{
	UIButtonsPressTimer = millis();
}

// External Declarations
// Public Methods
// Details of the method interfaces is available in UIButtons.h
void UIButtons::Begin(void)
{
	attachInterrupt((uint8_t)_UIButtonsInterruptNum, UIButtonsISR, (uint16_t)_UIButtonsInterruptMode);
}

UIButtons::UIButtonId_t UIButtons::WhichUIButtonPressed(void)
{
  // The UIButtonsPressTimer will be zero unless the interrupt has been triggered
  // A press is registered only after the debounce time has been waited and trigger pin is still LOW (to eliminate false press by bouncy releases)
  if ((UIButtonsPressTimer != 0) && ((millis() - UIButtonsPressTimer) > UIButtonDebounceTime_ms) && !digitalRead(_UIButtonsInterruptPin))
  {
	// The UIButtonsPressTimer is reset to zero so that no new presses will register until the interrupt is triggered again
    UIButtonsPressTimer = 0;
    for (uint16_t buttonIdx = 0; buttonIdx < NumberButtons; ++buttonIdx)
	{
		if (!digitalRead(_UIButtonPins[buttonIdx]))
		{
			return ((UIButtonId_t)buttonIdx);
		}
	}
  }
  return (NoButton);
}
