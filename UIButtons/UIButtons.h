#ifndef _UIBUTTONS_H_
#define _UIBUTTONS_H_
// Author:		Luke Spicer
// Date:		February 9th, 2013
// Purpose:		User Interface Button Class Declaration (Header)
// Description:	Declares a UIBottons class for use with a custom 5 button user interface board
//			
#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include <inttypes.h>
#include <InterruptConstants.h>

class UIButtons {
	public:
		///
		// Constants
		//
		static const uint32_t UIButtonDebounceTime_ms = 50; // Time a button must be held pressed before it is registered (to filter out bounce)
		typedef enum {
			UpButton		= 0,
			LeftButton		= 1,
			RightButton		= 2,
			DownButton 		= 3,
			SelectButton	= 4,
			NumberButtons	= 5,	// The total number of buttons on the UIBoard
			NoButton 		= 99,	// Code used to indicate that no button was pressed (zero is a valid button value)
		} UIButtonId_t;
	
		///
		// Constructor
		// Parameters:	interrupt number (interrupt numbers defined in Interrupt_types.h)
		//				interrupt mode (interrupt modes defined in Interrupt_types.h)
		//				<button>ButtonPin (pin number connected to each pin)
		// Description: Connects the hardware encoder to the Arduino interrupt hardware and GPIO
		// Result:		Interrupt will be attached and the module will begin indicating button presses
		// UIButtons(	InterruptNumber_t interNum, InterruptMode_t interMode,
					// int16_t upButtonPin, int16_t leftButtonPin, int16_t rightButtonPin,
					// int16_t downButtonPin, int16_t selectButtonPin);
		UIButtons(InterruptConstants::InterruptNumber_t interNum, InterruptConstants::InterruptMode_t interMode,
			int16_t upButtonPin, int16_t leftButtonPin, int16_t rightButtonPin,
			int16_t downButtonPin, int16_t selectButtonPin):	_UIButtonsInterruptNum(interNum),
																_UIButtonsInterruptMode(interMode)
		{
			_UIButtonPins[UpButton] = upButtonPin;
			_UIButtonPins[LeftButton] = leftButtonPin;
			_UIButtonPins[RightButton] = rightButtonPin; 
			_UIButtonPins[DownButton] = downButtonPin;
			_UIButtonPins[SelectButton] = selectButtonPin;
			_UIButtonsInterruptPin = GetInterruptPin(interNum);
		}
		
		///
		// Begin
		// Return:		void
		// Parameters:	void
		// Description: Attaches the UIButtons object to the UIButtons ISR
		void Begin(void);
		
		///
		// WhichUIButtonPressed
		// Return: 		The UIButton that was registered as pressed
		// Parameters:	void
		// Description: Possess the logic to determine which button was pressed, based on the UIButton ISR
		//				Call this function at a frequency greater than millis() rollover (~daily)
		UIButtonId_t WhichUIButtonPressed(void);
	
	private:
		InterruptConstants::InterruptNumber_t _UIButtonsInterruptNum;
		InterruptConstants::InterruptMode_t _UIButtonsInterruptMode;
		uint16_t _UIButtonsInterruptPin;
		uint16_t _UIButtonPins[NumberButtons];
		
};	// End Class UIButtons definition

#endif
