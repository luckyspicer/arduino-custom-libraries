/* Modified Arduino Uart Interface JPEG Camera Library
 * Luke Spicer 6/8/2011
 * Modified from:
 * Arduino JPEGCamera Library
 * Copyright 2010 SparkFun Electronic
 * Written by Ryan Owens
*/

#ifndef CameraUart_h
#define CameraUart_h

#include <avr/pgmspace.h>
#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif
#include "SD.h"

#define LOW 	1
#define	QVGA	2
#define	VGA		3

class CameraUart
{
	public:
		CameraUart();
		void begin(void);
		void reset(void);
		void readSize(void);
		int getSize(void);
		void setImageSize(int resolution);
		void takePicture(void);
		int savePicture(File imageFile);
		void stopPictures(void);
		void enterPowerSaving(void);
		void exitPowerSaving(void);
		int getLastResponseCount();
		char * getLastResponse();
		
	private:
		int readData(char * response, int address);
		int sendCommand(const char * command, char * response, int length);
		long sizeImage;
		unsigned int lastResponseCount;
		char lastResponse[32];
		
};

#endif