/* Modified Arduino Uart Interface JPEG Camera Library
 * Luke Spicer 6/8/2011
 * Modified from:
 * Arduino JPEGCamera Library
 * Copyright 2010 SparkFun Electronic
 * Written by Ryan Owens
*/

#include "CameraUart.h"

//Commands for the LinkSprite Serial JPEG Camera
const char GET_SIZE[5] = {0x56, 0x00, 0x34, 0x01, 0x00};
const char RESET_CAMERA[4] = {0x56, 0x00, 0x26, 0x00};
const char TAKE_PICTURE[5] = {0x56, 0x00, 0x36, 0x01, 0x00};
const char STOP_TAKING_PICS[5] = {0x56, 0x00, 0x36, 0x01, 0x03};
const char SET_IMAGE_SIZE_160x120[9] = {0x56, 0x00, 0x31, 0x05, 0x04, 0x01, 0x00, 0x19, 0x22};
const char SET_IMAGE_SIZE_320x240[9] = {0x56, 0x00, 0x31, 0x05, 0x04, 0x01, 0x00, 0x19, 0x11};
const char SET_IMAGE_SIZE_640x480[9] = {0x56, 0x00, 0x31, 0x05, 0x04, 0x01, 0x00, 0x19, 0x00};
const char ENTER_POWER_SAVING[7] = {0x56, 0x00, 0x3E, 0x03, 0x00, 0x01, 0x01};
const char EXIT_POWER_SAVING[7] = {0x56, 0x00, 0x3E, 0x03, 0x00, 0x01, 0x00};
char READ_DATA[8] = {0x56, 0x00, 0x32, 0x0C, 0x00, 0x0A, 0x00, 0x00};

//We read data from the camera in chunks, this is the chunk size
const int read_size=32;

//******************************Public Methods*************************************//

CameraUart::CameraUart()
{
}

//Initialize the serial connection
void CameraUart::begin(void)
{
	//Camera baud rate is 38400
	Serial1.begin(38400);
	sizeImage = 0;
}

//Reset the camera
//pre: lastResponse is an empty string
//	   lastResponseCount = 0;
//post: lastResponse contains the cameras response to the RESET_CAMERA command
//		lastResponseCount = number of characters in the response
//returns: void
//Usage: camera.reset(response);
void CameraUart::reset()
{
	lastResponseCount = sendCommand(RESET_CAMERA, lastResponse, 4);
}

//Get the size of the image currently stored in the camera
//pre: lastResponse is an empty string. size is a pointer to an integer
//	   lastResponseCount = 0;
//post: lastResponse string contains the entire camera response to the GET_SIZE command
//		sizeImage is set to the size of the image
//		lastResponseCount = number of bytes in the response
//return: void
//usage: length = camera.getSize(response, &img_size);
void CameraUart::readSize()
{
	int count=0;
	//Send the GET_SIZE command string to the camera
	count = sendCommand(GET_SIZE, lastResponse, 5);
	//Read 4 characters from the camera and add them to the response string
	for(int i=0; i<4; i++)
	{
		while(!Serial1.available());
		lastResponse[count+i]=Serial1.read();
	}
	//Set the number of characters to return
	count+=4;
	//The size is in the last 2 characters of the response.
	//Parse them and convert the characters to an integer
    sizeImage = ((int)lastResponse[count-2] & 0x00FF )*256;
	//Serial.print("H =");
	//Serial.println((int)lastResponse[count-2] & 0x00FF);
    sizeImage += (int)lastResponse[count-1] & 0x00FF;
	//Serial.print("L =");
	//Serial.println((int)lastResponse[count-1] & 0x00FF);
	//Send the number of characters in the response back to the calling function
	lastResponseCount = count;
}

//This function is simply gives us the value in the private int which contais
//The size of the image stored in the camera
int CameraUart::getSize()
{
	return sizeImage;
}

//Change the picture size
//pre: lastResponse is an empty string
//	   lastResponseCount = 0;
//post: lastResponse contains the cameras response to the SET_IMAGE_SIZE_XX command
//		lastResponseCount = number of characters in the response
//returns: void
//Usage: camera.changeImagesize(image_size)
void CameraUart::setImageSize(int resolution) {
	
	switch (resolution) {
	case LOW:
		lastResponseCount = sendCommand(SET_IMAGE_SIZE_160x120, lastResponse, 9);
    break;
	case QVGA:
		lastResponseCount = sendCommand(SET_IMAGE_SIZE_320x240, lastResponse, 9);
		break;
	case VGA:
		lastResponseCount = sendCommand(SET_IMAGE_SIZE_640x480, lastResponse, 9);
    break;
	default:
		lastResponseCount = sendCommand(SET_IMAGE_SIZE_640x480, lastResponse, 9);
	break;
  }
}

//Take a new picture
//pre: lastResponse is an empty string
//	   lastResponseCount = 0;
//post: lastResponse contains the cameras response to the TAKE_PICTURE command
//		lastResponseCount = number of characters in the response
//returns: void
//Usage: camera.takePicture(response);
void CameraUart::takePicture()
{
	lastResponseCount = sendCommand(TAKE_PICTURE, lastResponse, 5);
}

//Saves the picture thats in the Camera's buffer to the SD card
// file "imageFile" which the user can specify in their program
// That's all you need to know to make this thing work!
int CameraUart::savePicture(File imageFile) {
	if (!imageFile || !sizeImage) {
		return 0;
	}
	//Starting at address 0, keep reading data until we've read 'size' data.
	int address = 0;
	int count = 0;
	int eof =0;
	uint8_t response[256];
	while(address < sizeImage)
	{
		for(int j = 0; j<8; j++) {
			//Read the data starting at the current address.
			count=readData(lastResponse, address);
			//Store all of the data that we read to the SD card
			//count = 32;
			for(int i=0; i<count; i++){
				//Serial.println("Entered the for loop");
				//Check the response for the eof indicator (0xFF, 0xD9)
				//If we find it, set the eof flag
				if((lastResponse[i] == (char)0xD9) && (lastResponse[i-1]==(char)0xFF)) {
					eof=1;
				}
				//Save the data to the SD card
				//imageFile.print(lastResponse[i], BYTE);
				//imageFile.write(lastResponse[i]);
				//If we found the eof character, get out of this loop and stop reading data
				if(eof==1) {
					break;
				}
				response[i+(j*count)] = lastResponse[i];
			}
			address+=count;
			delay(2);
			if(eof==1) {
				break;
			}
		}
	    imageFile.write(response, count*8);
		
		//Increment the current address by the number of bytes we read
		//address+=count;
		//Make sure we stop reading data if the eof flag is set.
		if(eof==1) {
			break;
		}
	}
	return 1;
}

//Stop taking pictures
//pre: lastResponse is an empty string
//	   lastResponseCount = 0;
//post: lastResponse contains the cameras response to the STOP_TAKING_PICS command
//		lastResponseCount = number of characters in the response
//returns: void
//Usage: camera.stopPictures();
void CameraUart::stopPictures()
{
	lastResponseCount = sendCommand(STOP_TAKING_PICS, lastResponse, 5);
}

//Enter Power Saving Mode
//pre: lastResponse is an empty string
//	   lastResponseCount = 0;
//post: lastResponse contains the cameras response to the reset command
//		lastResponseCount = number of characters in the response
//returns: void
//Usage: camera.enterPowerSaving() 
void CameraUart::enterPowerSaving() {
	lastResponseCount = sendCommand(ENTER_POWER_SAVING, lastResponse, 7);
}

//Exit Power Saving Mode
//pre: lastResponse is an empty string
//	   lastResponseCount = 0;
//post: lastResponse contains the cameras response to the EXIT_POWER_SAVING command
//		lastResponseCount = number of characters in the response
//returns: void
//Usage: camera.exitPowerSaving()
void CameraUart::exitPowerSaving() {
	lastResponseCount = sendCommand(EXIT_POWER_SAVING, lastResponse, 7);
}

//Getter method to return the address of the response
// to the last command the camera was sent, can be used to print
//the camera's response to the serial terminal
char* CameraUart::getLastResponse() {
	return (char*)lastResponse;
}

//Getter method to return the private int which contains
//The number of characters returned in the response the camera
//gave to the last command it was sent, can be used to print
//the camera's response to the serial terminal
int CameraUart::getLastResponseCount() {
	return lastResponseCount;
}

//**************************Private Methods*********************************//

//Get the read_size bytes picture data from the camera
//pre: response is an empty string, address is the address of data to grab
//post: response contains the cameras response to the readData command, but the response header is parsed out.
//returns: number of characters in the response
//Usage: camera.readData(response, cur_address);
//NOTE: This function must be called repeatedly until all of the data is read
//See Sample program on how to get the entire image.
int CameraUart::readData(char * response, int address)
{
	int count=0;

	//Flush out any data currently in the serial buffer
	Serial1.flush();
	
	//Send the command to get read_size bytes of data from the current address
	for(int i=0; i<8; i++)Serial1.print(READ_DATA[i]);
	Serial1.write(address>>8);
	Serial1.write(address);
	Serial1.write(0x00);
	Serial1.write(0x00);
	Serial1.write(read_size>>8);
	Serial1.write(read_size);
	Serial1.write(0x00);
	Serial1.write(0x0A);	
	
	//Print the data to the serial port. Used for debugging.
	/*
	for(int i=0; i<8; i++)Serial.print(READ_DATA[i]);
	Serial.print(address>>8, BYTE);
	Serial.print(address, BYTE);
	Serial.print(0x00, BYTE);
	Serial.print(0x00, BYTE);
	Serial.print(read_size>>8, BYTE);
	Serial.print(read_size, BYTE);
	Serial.print(0x00, BYTE);
	Serial.print(0x0A, BYTE);	
	Serial.println();
	*/

	//Read the response header.
	for(int i=0; i<5; i++){
		while(!Serial1.available());
		Serial1.read();
	}
	
	//Now read the actual data and add it to the response string.
	count=0;
	while(count < read_size)
	{
		while(!Serial1.available());
		*response++=Serial1.read();
		count+=1;
	}
	
	//Return the number of characters in the response.
	return count;
}

//Send a command to the camera
//pre: Serial1 is a serial connection to the camera set to 3800 baud
//     command is a string with the command to be sent to the camera
//     response is an empty string
//	   length is the number of characters in the command
//post: response contains the camera response to the command
//return: the number of characters in the response string
//usage: None (private function)
int CameraUart::sendCommand(const char * command, char * response, int length)
{
	char c=0;
	int count=0;
	//Clear any data currently in the serial buffer
	Serial1.flush();
	//Send each character in the command string to the camera through the camera serial port
	for(int i=0; i<length; i++){
		Serial1.print(*command++);
	}
	//Get the response from the camera and add it to the response string.
	for(int i=0; i<length; i++)
	{
		while(!Serial1.available());
		*response++=Serial1.read();	
		count+=1;
	}
	
	//return the number of characters in the response string
	return count;
}
