#ifndef VFD_h
#define VFD_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif
#include <inttypes.h>
#include "Print.h"

#define PARALLEL 5
#define SERIAL0 0
#define SERIAL1 1
#define SERIAL2 2
#define SERIAL3 3

//VFD Functions, VFD inherits Print() function and its variants
//from Print.h and Print.cpp in the arduino core library
class VFD : public Print {
//Publicly available (to Arduino code) functions      
public:
  VFD(int controlNum, uint8_t wr, uint8_t rd,
		uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3,
		uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7);
  VFD(int serialNum);
  void backspace();
  void tab();
  void lineFeed();
  void home();
  void carriageReturn();
  void clear();
  void setCursor(uint8_t X, uint8_t Y);
  void cursorOn();
  void cursorOff();
  void brightness(uint8_t n);
  void invertOn();
  void invertOff();
  void charWidth(uint8_t w);
  void charMagnify(uint8_t x, uint8_t y);
  void blink(uint8_t p, unsigned long nT, unsigned long brT, unsigned long t);
  void scroll(unsigned long num, unsigned long reps, unsigned long t);
  void displayImage(uint8_t imageData[], uint8_t x, uint8_t y);
  void newLine();   
  virtual size_t write(uint8_t);
  
private:
  void begin(uint8_t wr, uint8_t rd,
	    uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3,
	    uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7);
  void begin(int serialNum);
  void init();
        
  uint8_t _rd_pin;			// read PBUSY flag when rd = 0, wr = 1
  uint8_t _wr_pin;			// controlls writing data on rising edge wr, rd = 1
  uint8_t _data_pins[8];	// Parallel Data transmitted over _data_pins
  uint8_t _sbusy_pin;		// Pin associated with serial busy signal
  uint8_t _controlNumber;	// Number for the serial/Parallel used to control display
};

#endif
