% clear the matlab screen
clc

% read orignial image bmp file and invert to get 1 where display will light
original_image = imread('C:\Users\Luke\Documents\Robotics\2011 Robot Competition\Display Module\N.bmp', 'BMP');
imwrite(original_image,'\linesPixel.pbm', 'PBM')
pixel_image = imread('\linesPixel.pbm', 'PBM')
inverted_image = 1. - pixel_image
[y, x] = size(inverted_image);

% x and y demensions of the image in pixels (for the VFD display the
% maximum is 140x32
Horizontal_Pixels = x
Vertical_Pixesl = y
% generate a padding matrix to pad the y demension to a multiple of 8 (for the vfd display module)
padding_matrix = zeros(0, 0);
rows_flag = 0;
if mod(y, 8)~=0
    if y < 8
        padding_matrix = zeros(8 - y, x)
        rows_flag = 1;
    end
    if (y > 8 && y < 16)
        padding_matrix = zeros(16 - y, x)
        rows_flag = 2;
    end
    if (y > 16 && y < 24)
        padding_matrix = zeros(24 - y, x)
        rows_flag = 3;
    end
    if (y > 24 && y < 32)
        padding_matrix = zeros(32 - y, x)
        rows_flag = 4;
    end
end

% concatonate padding_matrix to inverted_image to generate a proper sized image
% for the rest of the processing and output
resized_image = vertcat(inverted_image, padding_matrix);

% generate HEX codes for each column
nbit=2.^(size(resized_image,1)-1:-1:0);

% Hex Output!
Hex_Output=dec2hex(nbit*resized_image);
Hex_Output = Hex_Output'

Hex_Output = reshape(Hex_Output, 2, size(Hex_Output,1)/2*size(Hex_Output, 2));
OUT={Hex_Output};

% Place HEX output in a text file labeled outImage
HEX_PREFIX = ' 0x';
outfile = fopen('C:\Users\Luke\Documents\Robotics\2011 Robot Competition\Display Module\outImage.txt', 'w');
fprintf(outfile, 'IMAGE = {');
for i=1:1:size(Hex_Output, 2)
    fprintf(outfile, '%s', strcat(HEX_PREFIX, Hex_Output(1,i), Hex_Output(2,i)));
    if i ~= size(Hex_Output, 2)
        fprintf(outfile, ',');
    end
    if mod(i, 16) == 0
        fprintf(outfile, '\n');
    end
end
fprintf(outfile, ' }');
fclose('all');