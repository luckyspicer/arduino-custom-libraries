#include "VFD.h"

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

 /* Constructor for Parallel controlled VFD object.
  * Initializes the display and sets up data
  * and control pins
  */
 VFD::VFD(int controlNum, uint8_t wr, uint8_t rd,
                  uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3,
                  uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7)
  {
	// This check is simply to ensure that the user is aware that they're using a
	// Parallel controlled VFD
	if (controlNum == 5) {
		//_controlNumber = 5, means use parallel interface
		_controlNumber = 5;
	}
	//begin function sets up data and control pins and initializes the display
	begin(wr, rd, d0, d1, d2, d3, d4, d5, d6, d7);
  }
  
 /* Constructor for Serial VFD object.
  * Initializes the display and sets up
  * the serial port to use
  */
 VFD::VFD(int serialNum)
  {
	//begin function initializes the display
	begin(serialNum);
  }

/* PARALLEL VFD begin function
 * Function that assigns pins to each of the data lines and to both of the 
 * control lines. Then the function calls init() which initializes the display
 * to its default settings
 */ 
 void VFD::begin(uint8_t wr, uint8_t rd,
			 uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3,
			 uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7)
 {		
	   //wr writes data on rising edged, rd used to read PBUSY flag
       _wr_pin = wr;
       _rd_pin = rd;
       
       //8-bit data lines, pins user selected in the constructor
       _data_pins[0] = d0;
       _data_pins[1] = d1;
       _data_pins[2] = d2;
       _data_pins[3] = d3; 
       _data_pins[4] = d4;
       _data_pins[5] = d5;
       _data_pins[6] = d6;
       _data_pins[7] = d7;
       
       //wr, rd set to output since they control the display
       pinMode(_wr_pin, OUTPUT);
       pinMode(_rd_pin, OUTPUT);
       
       //Reset the display to all its defualt settings
       init();
 }
 
/* SERIAL VFD begin function
 * Setups the chosen serial communication port
 * Then the function calls init() which initializes the display
 * to its default settings
 */ 
 void VFD::begin(int serialNum)
 {
       switch (serialNum) {
		case 0:
			Serial.begin(38400);
			_controlNumber = 0;
			_sbusy_pin = 0;
			break;
		case 1:
			Serial1.begin(38400);
			_controlNumber = 1;
			_sbusy_pin = 19;
			break;
		case 2:
			Serial2.begin(38400);
			_controlNumber = 2;
			_sbusy_pin = 17;
			break;
		case 3:
			Serial3.begin(38400);
			_controlNumber = 3;
			_sbusy_pin = 19;
			break;
       }
       
       //Reset the display to all its defualt settings
       init();
 }

/********************
 * PUBLIC FUNCTIONS *
 ********************
 */
 
 //Native Noritake 7000 series functions
 
 //Move cursor back by one character
 void VFD::backspace() {
      write(0x08);
 }
 
 //Move cursor forward by one character
 void VFD::tab() {
      write(0x09);
 }
 
 //Move cursor to one lower line
 void VFD::lineFeed() {
      write(0x0A);
 }
 
 //Move cursor to home position
 void VFD::home() {
      write(0x0B);
 }
 
 //Move cursor to left end of the same line
 void VFD::carriageReturn() {
      write(0x0D);
 }
 
 //Clear the display and move cursor home
 void VFD::clear() {
      write(0x0C);
 }
 
 //Move cursor to specified X-Y position
 //X position is the horizontal cursor position in number of dots
 //X default is 0, X max is 140 for visible display
 //Y position is the vertical curosr position (line position)
 //Y default is 0, y max is 3
 void VFD::setCursor(uint8_t X, uint8_t Y) {
      if(X > 140 || Y > 3){
           X = 0;
           Y = 0;
      } 
      write(0x1F);
      write(0x24);
      write(X);
      write(0x00);
      write(Y);
      write(0x00);
 }
 
 //Make cursor visible on display
 void VFD::cursorOn() {
      write(0x1F);
      write(0x43);
      write(0x01);
 }
 
 //Disable Cursor (make it invisible)
 void VFD::cursorOff() {
      write(0x1F);
      write(0x43);
      write(0x00);
 }
 
 //Set the Displays brightness level
 //n Level (default is 8)
 //1 12.5% 2 25%
 //3 37.5% 4 50%
 //5 62.5% 6 75%
 //7 87.5% 8 100%
 void VFD::brightness(uint8_t n) {
      if(n > 8) {
           n = 8;
      }
      write(0x1F);
      write(0x58);
      write(n);
 }
 
 //Invert Display (background lit, characters off)
 //Invert only applies to data written after it is called
 void VFD::invertOn() {
      write(0x1F);
      write(0x72);
      write(0x01);     
 }
 
 //Set reverse display off, restore to default
 //Only applies to data written after is is called
 void VFD::invertOff() {
      write(0x1F);
      write(0x72);
      write(0x00);
 }
 
 //Change the font width
 // w = 0: Fixed Character Width 1 (1 dot space in right side)
 // w = 1: Fixed Character Width 2 (1 dot space in each right and left side)
 // w = 2: Proportional Character Width 1 (1 dot space in right side)
 // w = 3: Proportional Character Width 2 (1 dot space in each right and left side)
 // w = 0 is default
 void VFD::charWidth(uint8_t w) {
      if(w > 3) {
           w = 0;
      }
      write(0x1F);
      write(0x28);
      write(0x67);
      write(0x03);
      write(w);
 }
 
 // Magnify Characters
 // x specifies the size of the horizontal magnification
 // y specifies the size of the vertical magnification
 void VFD::charMagnify(uint8_t x, uint8_t y) {
      if(x > 4 || y > 2 || x < 1 || y < 1) {
           x = 1;
           y = 1;
      }
      write(0x1F);
      write(0x28);
      write(0x67);
      write(0x40);
      write(x);
      write(y);
 }
 
 // Blink the display
 // p specifies blink pattern
 // p=0 (normal display), p=1 (normal/blank), p=2 (normal/reverse)
 // nT = time in mSec for Normal display for each blink
 // brT = time in mSec for blank or reverse display for each blink
 // t = total time in mSec for display to blink
 void VFD::blink(uint8_t p, unsigned long nT, unsigned long brT, unsigned long t) {
      // If invalid blink pattern is picked go with default
      if(p < 0 || p > 2) {
           p = 0;
      }
      // Calculate normal and blank/reverse times for VFD command
      uint8_t t1 = (nT/14);
      uint8_t t2 = (brT/14);
      if(t1 > 255 || t1 < 1 || t2 > 255 || t2 < 1 ) {
            t1 = 1;
            t2 = 1;
      }
      
      // Calculate Total time for blinking and convert to proper VFD value
      if(t < (nT + brT)) {
           t = (nT + brT);
      }
      uint8_t c = t/(nT + brT);
      if(c > 255) {
           c = 255;
      }
      
      write(0x1F);
      write(0x28);
      write(0x61);
      write(0x11);
      
      //blink pattern
      write(p);
      //normal time
      write(t1);
      //reverse or blank time
      write(t2);
      //number of repetitions
      write(c);
  }
  
 // Scroll the display
 // num = the number of dots to shift the display each shift
 // reps = the number shifts to perform
 // t = time per shift in mSec
 void VFD::scroll(unsigned long num, unsigned long reps, unsigned long t) {
      // Convert number of dots per shift to form used by VFD commands
      // Max number of dots per shift = 1023
      if(num > 1023) {
             num = 1023;
      }
      uint8_t wH = num/256;
      uint8_t wL = num % 256;
      
      // Convert number shifts to perform to form used by VFD commands
      // Max number of shifts per call = 65535
      if(reps > 65535) {
             reps = 65535;
      }
      uint8_t cH = reps/256;
      uint8_t cL = reps % 256;
      
      // convert to amount of time per shift in multiples of 14mSec
      // Max time per shift is 255x14msec = 3.57sec, Min time per shift = 14mSec
      uint8_t s = t/14;
      if(s > 255) {
           s = 255;
      }
      if(s == 0) {
           s = 1;
      }
   
      // VFD Required Scroll Commands
      write(0x1F);
      write(0x28);
      write(0x61);
      write(0x10);
      
      // Number of dots to shift display, lower then upper bytes
      write(wL);
      write(wH);
      
      // Number of shifts per call, lower then upper bytes
      write(cL);
      write(cH);
      
      // Time per shift = s*14msec
      write(s);
  }
 
 // Display bitmap images, using the bitmap array generator written in matlab
 // Input the name of the bitmap array and the x and y dimensions in pixels
 void VFD::displayImage(uint8_t imageData[], uint8_t x, uint8_t y) {
      // Convert x and y dimensions to proper sizes for the display
      if (x > 140) {
            x = 140;
      }
      if (x <= 0 ) {
            x = 1;
      }
      
      if (y <= 8 ) {
            y = 1;
      }
      else if (y > 8 && y <= 16) {
            y = 2;
      }
      else if (y > 16 && y <= 24) {
            y = 3;
      }
      else if (y > 24 && y <= 32) {
            y = 4;
      }
       
      //required hex codes for displaying a bitmap
      write(0x1F);
      write(0x28);
      write(0x66);
      write(0x11);
  
      // x dimension (width) in pixels Lower Byte then Higher Byte
      write(x);
      write(0x00);
      
      // y dimension (hight) in pixels Lower Byte then Higher Byte
      write(y);
      write(0x00);
  
      write(0x01);
      for(int i = 0; i < (x*y); i++) {
              write(imageData[i]);}
  }
           
 //Compound Display Functions (written by Luke Spicer)
 
 //Move cursor to the beginning of the next line
 void VFD::newLine() {
      lineFeed();
      carriageReturn();     
}

  /* Refer to the Noritake GU140x32F 7000 datsheet, page 5 for
   * specifics about the parallel data/command interface
   * wr must be LOW, rd HIGH when command or data loaded. Rising edge
   * on wr writes data or command to LCD. Sending rd low allows PBUSY flag
   * to be read on VFD data pin 7. PBUSY is high while display processes
   * the data. Wait for PBUSY to be low before leaving subroutine.
   *
   * If using a Serial port, SBUSY must be low to return from write command
   */
 inline size_t VFD::write(uint8_t value) {
		if((_controlNumber >= 0) && (_controlNumber <= 3)) {
		   switch (_controlNumber) {
			case 0:
				Serial.write(value);
				break;
			case 1:
				Serial1.write(value);
				break;
			case 2:
				Serial2.write(value);
				break;
			case 3:
				Serial3.write(value);
				break;
		   }
		   //_sbusy_pin set to INPUT so SBUSY can be read
		   pinMode(_sbusy_pin, INPUT);
		   //subroutine waits for SBUSY to go low before returning
		   while(digitalRead(_sbusy_pin) == HIGH) {
		   }
		}
		else if(_controlNumber == 5) {
		   //wr LOW, rd HIGH to write data or command to display
		   digitalWrite(_wr_pin, LOW);                               
		   digitalWrite(_rd_pin, HIGH);
		   
		   //The data or command is loaded bitwise onto the data pin
		   for (int i = 0; i <= 7; i++) {
               //pins are make into OUTPUTS before writing data to them
               pinMode(_data_pins[i], OUTPUT);
               digitalWrite(_data_pins[i], (value >> i) & 0x01);
		   }
		   
		   //wr set HIGH, rising edged of wr writes data or command to display
		   digitalWrite(_wr_pin, HIGH);
		   //rd set LOW so that PBUSY flag can be read on data pin 7
		   digitalWrite(_rd_pin, LOW);
		   
		   //pin 7 is set to INPUT so PBUSY can be read
		   pinMode(_data_pins[7], INPUT);
		   //subroutine waits for PBUSY to go low before returning
		   while(digitalRead(_data_pins[7]) == HIGH) {
		   }
		}
 }

 /********************
 * PRIVATE FUNCTIONS *
 *********************
 */
 
 //Reset display to default settings
 void VFD::init() {
      write(0x1B);
      write(0x40);
 }
