#ifndef _L3GD20_GYRO_H_
#define _L3GD20_GYRO_H_

#include <Arduino.h> // for byte data type

// device types

#define L3G_DEVICE_AUTO 0
#define L3G4200D_DEVICE 1
#define L3GD20_DEVICE   2

// SA0 states

#define L3G_SA0_LOW  0
#define L3G_SA0_HIGH 1
#define L3G_SA0_AUTO 2

// register addresses

#define L3G_WHO_AM_I      0x0F

#define L3G_CTRL_REG1     0x20
#define L3G_CTRL_REG2     0x21
#define L3G_CTRL_REG3     0x22
#define L3G_CTRL_REG4     0x23
#define L3G_CTRL_REG5     0x24
#define L3G_REFERENCE     0x25
#define L3G_OUT_TEMP      0x26
#define L3G_STATUS_REG    0x27

#define L3G_OUT_X_L       0x28
#define L3G_OUT_X_H       0x29
#define L3G_OUT_Y_L       0x2A
#define L3G_OUT_Y_H       0x2B
#define L3G_OUT_Z_L       0x2C
#define L3G_OUT_Z_H       0x2D

#define L3G_FIFO_CTRL_REG 0x2E
#define L3G_FIFO_SRC_REG  0x2F

#define L3G_INT1_CFG      0x30
#define L3G_INT1_SRC      0x31
#define L3G_INT1_THS_XH   0x32
#define L3G_INT1_THS_XL   0x33
#define L3G_INT1_THS_YH   0x34
#define L3G_INT1_THS_YL   0x35
#define L3G_INT1_THS_ZH   0x36
#define L3G_INT1_THS_ZL   0x37
#define L3G_INT1_DURATION 0x38

#define L3G_DATA_START	  L3G_OUT_TEMP

class L3GD20
{
  public:
    typedef struct gyroData_s
    {
    	int8_t temp;		// To convert temp to degrees C: C ~= (175 - temp) / 2
    	uint8_t status;
    	int16_t x;
    	int16_t y;
    	int16_t z;
    	gyroData_s(): temp(0), status(0), x(0), y(0), z(0){}
    } gyroData_t;

    typedef enum gyroStatus_e
    {
    	L3G_XDA =	0x01,
    	L3G_YDA =	0x02,
    	L3G_ZDA =	0x04,
    	L3G_ZYXDA = 0x08,
    	L3G_XOR =	0x10,
    	L3G_YOR	=	0x20,
    	L3G_ZOR =	0x40,
    	L3G_ZYXOR =	0x80
    } gyroStatus_t;

    L3GD20();
    bool Init(void);
    void EnableDefault(void) const;
    void WriteReg(uint8_t reg, uint8_t value) const;
    uint8_t ReadReg(uint8_t reg) const;

    void ReadData(void);
    void GetGyroData(gyroData_t &gyroData) const;

  private:
    static const uint8_t BytesOfGyroData = sizeof(gyroData_t);
    static const uint8_t SizeofReg = sizeof(uint8_t);
    gyroData_t _gyroData; 	// gyro angular velocity readings
    uint8_t _address;

    bool AutoDetectAddress(void);
};

#endif



