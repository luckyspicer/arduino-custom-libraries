#include <Wire.h>
#include <L3GD20.h>
#include <DuinOS/DuinOS.h>

// Defines ////////////////////////////////////////////////////////////////

// The Arduino two-wire interface uses a 7-bit number for the address,
// and sets the last bit correctly based on reads and writes
#define L3GD20_ADDRESS_SA0_LOW    (0xD4 >> 1)
#define L3GD20_ADDRESS_SA0_HIGH   (0xD6 >> 1)

// Public Methods //////////////////////////////////////////////////////////////

L3GD20::L3GD20(): _gyroData(), _address(0) {}

bool L3GD20::Init(void)
{
	return (AutoDetectAddress());
}

// Turns on the L3GD20 and places it in normal mode.
void L3GD20::EnableDefault(void) const
{
  // 0x0F = 0b00001111
  // Normal power mode, all axes enabled
  WriteReg(L3G_CTRL_REG1, 0x0F);
}

// Writes a gyro register
void L3GD20::WriteReg(uint8_t reg, uint8_t value) const
{
  Wire.beginTransmission(_address);
  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();
}

// Reads a gyro register
uint8_t L3GD20::ReadReg(uint8_t reg) const
{
  uint8_t value;

  Wire.beginTransmission(_address);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(_address, SizeofReg);
  value = Wire.read();
  Wire.endTransmission();

  return (value);
}

// Reads the 3 gyro channels and stores them in vector g
void L3GD20::ReadData()
{
  Wire.beginTransmission(_address);
  // assert the MSB of the address to get the gyro
  // to do slave-transmit sub-address updating.
  Wire.write(L3G_DATA_START | (1 << 7));
  Wire.endTransmission();
  Wire.requestFrom(_address, BytesOfGyroData);

  // Wait until all the data we requested is available
  while (Wire.available() < BytesOfGyroData)
  {
	  vTaskDelay(1); // release the processor to do some work for 1mSec if waiting on Data
  }

  int8_t temp = Wire.read();
  uint8_t status = Wire.read();
  uint8_t xlg = Wire.read();
  uint8_t xhg = Wire.read();
  uint8_t ylg = Wire.read();
  uint8_t yhg = Wire.read();
  uint8_t zlg = Wire.read();
  uint8_t zhg = Wire.read();

  _gyroData.temp = temp;
  _gyroData.status = status;
  // combine high and low bytes of rate data
  _gyroData.x = (int16_t)((xhg << 8) | xlg);
  _gyroData.y = (int16_t)((yhg << 8) | ylg);
  _gyroData.z = (int16_t)((zhg << 8) | zlg);
}

void L3GD20::GetGyroData(gyroData_t &gyroData) const
{
	gyroData.temp = _gyroData.temp;
	gyroData.status = _gyroData.status;
	gyroData.x = _gyroData.x;
	gyroData.y = _gyroData.y;
	gyroData.z = _gyroData.z;
}

// Private Methods //////////////////////////////////////////////////////////////

bool L3GD20::AutoDetectAddress(void)
{
  // try each possible address and stop if reading WHO_AM_I returns the expected response
  _address = L3GD20_ADDRESS_SA0_LOW;
  if (ReadReg(L3G_WHO_AM_I) == 0xD4) return (true);
  _address = L3GD20_ADDRESS_SA0_HIGH;
  if (ReadReg(L3G_WHO_AM_I) == 0xD4) return (true);

  return (false);
}
