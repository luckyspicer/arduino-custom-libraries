/* Music Library Header file
 * Luke Spicer, March 17th, 2009
 * Use this Libary to play music/tones with
 * a piezo element and your Arduino!
 */

#ifndef Music_h    //these make sure the libary is only called once
#define Music_h

#if defined(ARDUINO) && ARDUINO >= 100 //this includes the stdlib and other arduino files
#include "Arduino.h"
#else
#include "WProgram.h"
#endif      

/*=============================================================================
 * To Play music with the piezo we must use the frequency of each
 * note.
 * I have defined 6 octaves of notes, if you wish to add others or comment
 * out the ones you don't need, feel free to do so.
===============================================================================
====== note/period     frequency ==============*/

typedef enum {
     C1  =	33, // Hz  C
     Cs1 =	35, // Hz  D flat / C sharp
     D1  =	37, // Hz  D
     Eb1 =	38, // Hz  D sharp / E flat
     E1  =	41, // Hz  E
     F1  =	44, // Hz  F
     Fs1 =	46, // Hz  G flat / F sharp
     G1  =	49, // Hz  G
     Gs1 =	52, // Hz  A flat / G sharp
     A_1  =	55, // Hz  A
     Bb1 =	58, // Hz  B flat
     Bn1 =	62, // Hz  B (natural)

     C2  =	65, // Hz  C
     Cs2 =	70, // Hz  D flat / C sharp
     D2  =	73, // Hz  D
     Eb2 =	78, // Hz  D sharp / E flat
     E2  =	82, // Hz  E
     F2  =	87, // Hz  F
     Fs2 =	93, // Hz  G flat / F sharp
     G2  =	98, // Hz  G
     Gs2 =	104, // Hz  A flat / G sharp
     A_2  =	110, // Hz  A
     Bb2 =	117, // Hz  B flat
     B2  =	123, // Hz  B

     C3  =	131, // Hz  C
     Cs3 =	138, // Hz  D flat / C sharp
     D3  =	147, // Hz  D
     Eb3 =	156, // Hz  D sharp / E flat
     E3  =	165, // Hz  E
     F3  =	175, // Hz  F
     Fs3 =	185, // Hz  G flat / F sharp
     G3  =	196, // Hz  G
     Gs3 =	208, // Hz  A flat / G sharp
     A_3  =	220, // Hz  A
     Bb3 =	233, // Hz  B flat
     B3  =	246, // Hz  B

     c4  =	261, // Hz ===================MIDDLE C======================
     cs4 =	277, // Hz  D flat / C sharp
     d4  =	294, // Hz  D
     eb4 =	311, // Hz  D sharp / E flat
     e4  =	329, // Hz  E
     f4  =	349, // Hz  F
     fs4 =	370, // Hz  G flat / F sharp
     g4  =	392, // Hz  G
     gs4 =	415, // Hz  A flat / G sharp
     a4  =	440, // Hz  A
     bb4 =	466, // Hz  B flat
     b4  =	493, // Hz  B

     c5  =	523, // Hz  C
     cs5 =	554, // Hz  D flat / C sharp
     d5  =	587, // Hz  D
     eb5 =	622, // Hz  D sharp / E flat
     e5  =	659, // Hz  E
     f5  =	698, // Hz  F
     fs5 =	740, // Hz  G flat / F sharp
     g5  =	784, // Hz  G
     gs5 =	831, // Hz  A flat / G sharp
     a5  =	880, // Hz  A
     bb5 =	932, // Hz  B flat
     b5  =	988, // Hz  B

     c6  =	1047, // Hz  C
     cs6 =	1109, // Hz  D flat / C sharp
     d6  =	1175, // Hz  D
     eb6 =	1245, // Hz  D sharp / E flat
     e6  =	1319, // Hz  E
     f6  =	1397, // Hz  F
     fs6 =	1480, // Hz  G flat / F sharp
     g6  =	1568, // Hz  G
     gs6 =	1661, // Hz  A flat / G sharp
     a6  =	1760, // Hz  A
     bb6 =	1865, // Hz  B flat
     b6  =	1976, // Hz  B

     c7  =	2093, // Hz  C
     cs7 =	2217, // Hz  D flat / C sharp
     d7  =	2349, // Hz  D
     eb7 =	2489, // Hz  D sharp / E flat
     e7  =	2637, // Hz  E
     f7  =	2794, // Hz  F
     fs7 =	2960, // Hz  G flat / F sharp
     g7  =	3136, // Hz  G
     gs7 =	3322, // Hz  A flat / G sharp
     a7  =	3520, // Hz  A
     bb7 =	3729, // Hz  B flat
     b7  =	3951, // Hz  B

     R   =	000         // R is a rest, no tone is played
} note_frequencies_t;

/*=============================================================================
* Now we define the Note durations (how long each not will play.
* If you wish to play a note of duration longer than these,
* or of some other multiple (like a dotted quarter note), just add
* them up.   Example: dotted quarter = q + e
*            Example: dotted eighth = e + s
==============================================================================*/

typedef enum {
	q = 100,  // Quarter Note
	h = 200,  // Half Note
	W = 400,  // Whole Note
	e = 50,  // Eighth Note
	s = 25   // Sixteenth Note
} note_durations_t;

/*=============================================================================
* Now we define the class of our library, that is Music
* Within this place we have a public constructor, called Music and a public
* method called playMusic which returns nothing but plays the tune we enter to 
* it as an integer array
==============================================================================*/
class Music
{
    public:
		Music(int speaker);          //constructor, setsup up the speaker pin
		typedef struct music_note_s {
			note_frequencies_t frequency;
			int duration;
		} music_note_t;
		typedef struct song_s {
			long int bpm;
			const music_note_t* notes;
			int num_notes;
			song_s(long int _bpm, const music_note_t* _notes, int _num_notes):
				bpm(_bpm), notes(_notes), num_notes(_num_notes){}
		} song_t;
        void playMusic(const song_t &song);  // method which plays the music
    private:
        static const unsigned long int MS_PER_MIN_100 = 600000;
        typedef enum {
        	NOT_PLAYING,
        	PREPARE_NOTE,
        	PLAYING,
        } music_player_state_t;
        int _speaker;                //private variable used in the Music class
        music_player_state_t music_player_state;
        int song_length;
        long song_tempo;
        int current_note;
        int current_frequency;
        unsigned int current_duration;
        unsigned int duration_timer_ms;
};

#endif
