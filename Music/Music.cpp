/* Music Library C++ file
 * Luke Spicer, March 17th, 2009
 * Use this Libary to play music/tones with
 * a piezo element and your Arduino!
 */

#include "Music.h"     //Include the header file Music.h so our library works
#include "StandardConstants.h"

/*=============================================================================
* This constructor begins the music class and initializes the speaker pin
* The input to Music() is an integer corresponding to a digital I/0 pin on the
* arduino board. This constructor sets up that pin as OUTPUT and defines our
* private variable from Music.h, _speaker, equal to our chosen pin
==============================================================================*/
//Class::Constructor(Datatype variable)
Music::Music(int speaker) : music_player_state(NOT_PLAYING),
							song_length(0), song_tempo(0), current_note(0),
							current_frequency(0), current_duration(0), duration_timer_ms(0)
{
    pinMode(speaker, OUTPUT);   //pinMode is an arduino standard function
    _speaker = speaker;
}

/*=============================================================================
* This is the fun part!!! Getting on off electrical pulses to sound like tones
* (and maybe even music if you wish). The general idea is that cycling On/Off
* the piezo element pin at the frequency we hear as a given note will produce
* that note. To do this the user will enter their song as an integer array. The
* first value in that array will be the BPM (beats per minute), to approximate
* tempo. After the BPM the following values will be alternating note names and
* and durations. Example: c4,q  (that's middle c held for a quarter note). The
* available note names and durations are listed in Music.h as #define constants.
*                                HAVE FUN!!!
==============================================================================*/
void Music::playMusic(const song_t &song)     //void means this method returns nothing
{
	switch (music_player_state) {
	default:
	case NOT_PLAYING:
		song_length = song.num_notes;       //use song array length and note size to get # of notes in song
		song_tempo = MS_PER_MIN_100 / song.bpm;    //tempo is determined by song[0] = BPM
		current_note = 0;
		music_player_state = PREPARE_NOTE;
		break;
	case PREPARE_NOTE:
		noTone(_speaker);
		if (current_note < song_length)
		{
			current_frequency = song.notes[current_note].frequency;             //the period of our note from Music.h
			current_duration = (long(song.notes[current_note].duration) * song_tempo) / MS_PER_SEC; //actual duration, from tempo
			music_player_state = PLAYING;
			duration_timer_ms = GetTimerMS();
		}
		else
		{
			music_player_state = NOT_PLAYING;
		}
		break;
	case PLAYING:
		if (ElapsedMS(duration_timer_ms, GetTimerMS()) < current_duration)
		{
			if(current_frequency == R)                   //If the period is zero, it's a rest                             //Else the note is not a rest, play it!
			{
			  noTone(_speaker);    //turn off the piezo after note completed
			}
			else
			{
			  tone(_speaker, current_frequency);
			}
		}
		else
		{
			music_player_state = PREPARE_NOTE;
			current_note++;
		}
		break;
	}
}
