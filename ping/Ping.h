#ifndef _PING_H_
#define _PING_H_

// Luke Spicer
// Spicer Robotics
// 2013-05-06

#include "Arduino.h"
#include "InterruptConstants.h"

class Ping {
public:
	typedef enum {
		StartPing,
		ContinuePing,
		EndPing,
	} PingCmd_t;
	static const uint32_t PulseMax_us = 18500;
	static const uint32_t PulseMin_us = 115;
	static const uint32_t TransitionTimeout_ms = 20;
	typedef void (*isrPtr)(void);
	Ping(InterruptConstants::InterruptNumber_t inter_num);
	void SendPing(PingCmd_t cmd);
	uint32_t GetPulseWidth(void) const;
	uint32_t GetPingState(void) const;

	typedef enum {
		WaitForStart,
		WaitForTrigger,
		WaitForRising,
		WaitForFalling,
		Done,
	} PingState_t;

	static double uSecondsToCM(long microseconds)
	{
	  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
	  // The ping travels out and back, so to find the distance of the
	  // object we take half of the distance travelled.
	  return (microseconds / 29.4 / 2.0);
	}

private:

	uint32_t interruptNum;
	int triggerPin;
	volatile uint32_t pulseTimer;
	volatile uint32_t pulseWidth;
	volatile PingState_t pingState;
};

#endif
