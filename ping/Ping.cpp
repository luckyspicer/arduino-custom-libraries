#include "Ping.h"

// Static (Local) Declarations
static void isrPing(void);
static Ping *localPingPtr = NULL;

Ping::Ping(InterruptConstants::InterruptNumber_t inter_num): 	interruptNum(inter_num), pulseTimer(0),
																pulseWidth(PulseMin_us), pingState(WaitForStart)
{
	triggerPin = InterruptConstants::GetInterruptPin(inter_num);
}

void Ping::SendPing(PingCmd_t cmd)
{
	if (cmd == StartPing)
	{
		pingState = WaitForTrigger;
	}
	switch (pingState)
	{
	case WaitForStart:
		detachInterrupt(interruptNum);
		break;
	case WaitForTrigger:
		localPingPtr = this;
		pulseTimer = 0;
		pulseWidth = PulseMin_us;
		pingState = WaitForRising;
		pinMode(triggerPin, OUTPUT);
		digitalWrite(triggerPin, LOW);
		delayMicroseconds(2);
		digitalWrite(triggerPin, HIGH);
		delayMicroseconds(5);
		digitalWrite(triggerPin, LOW);
		pinMode(triggerPin, INPUT);
		attachInterrupt(interruptNum, isrPing, InterruptConstants::Interrupt_Rising);
		break;
	case WaitForRising:
		pulseTimer = micros();
		pingState = WaitForFalling;
		attachInterrupt(interruptNum, isrPing, InterruptConstants::Interrupt_Falling);
		break;
	case WaitForFalling:
		pulseWidth = (micros() - pulseTimer);
		detachInterrupt(interruptNum);
		pingState = Done;
		break;
	case Done:
		if (cmd == EndPing)
		{
			pingState = WaitForStart;
		}
		break;
	}
}

uint32_t Ping::GetPulseWidth() const
{
	return ((pulseWidth > PulseMin_us) ? pulseWidth : 0);
}

uint32_t Ping::GetPingState() const
{
	return (pingState);
}

void isrPing()
{
	localPingPtr->SendPing(Ping::ContinuePing);
}
