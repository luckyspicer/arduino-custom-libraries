#include <InterruptConstants.h>

// Pass the interrupt number and the pin associated with that interrupt will be returned
uint16_t InterruptConstants::GetInterruptPin(const InterruptNumber_t interNum)
{
	uint16_t returnVal = 0;
	switch (interNum)
	{
	case Interrupt_Zero:
		returnVal = 2;
		break;
	case Interrupt_One:
		returnVal = 3;
		break;
// Pin change interrupts only available to Arduino Mega variants currently
#if defined(__AVR_ATmega1284P__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
	case Interrupt_Two:
		returnVal = 21;
		break;
	case Interrupt_Three:
		returnVal = 20;
		break;
	case Interrupt_Four:
		returnVal = 19;
		break;
	case Interrupt_Five:
		returnVal = 18;
		break;
#endif
	default:
		returnVal = 0;
		break;
	}
	return (returnVal);
}

// Pin change interrupts only available to Arduino Mega variants currently
#if defined(__AVR_ATmega1284P__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)

typedef enum
{
	PCI0 = 0,
	PCI1 = 1,
	PCI2 = 2,
	PIN_CHANGE_NUM_INTERRUPTS
} PinChangeInterruptCtrl_t;

typedef void (*voidFuncPtr)(void);
static volatile voidFuncPtr intFunc[PIN_CHANGE_NUM_INTERRUPTS] = {0};

void InterruptConstants::AttachPinChangeInterrupt(PinChangeInterruptNumber_t interruptNum, void (*userFunc)(void))
{
  if((interruptNum < NUM_PCINT) && (interruptNum != UNUSED_PICINT))
  {
	  uint16_t pciCtrlNum = (interruptNum / PIN_CHANGE_NUM_INTERRUPTS);	// There are only 3 separate pin change interrupt vectors
	  uint16_t pciMaskBit = (interruptNum % 8);							// The interrupt number corresponds to a bit of the PCMSKx register

	  noInterrupts(); // Start critical section (I'm messing with the interrupts!)
	  intFunc[pciCtrlNum] = userFunc;
	  switch(pciCtrlNum){
	  case PCI0:
		  PCMSK0 |= (1 << pciMaskBit);
		  break;
	  case PCI1:
		  PCMSK1 |= (1 << pciMaskBit);
		  break;
	  case PCI2:
		  PCMSK2 |= (1 << pciMaskBit);
		  break;
	  }
	  PCICR |= (1 << pciCtrlNum);
	  interrupts(); // End critical section (I'm done messing with the interrupts)
  }
}

void InterruptConstants::DetachPinChangeInterrupt(PinChangeInterruptNumber_t interruptNum)
{
  if((interruptNum < NUM_PCINT) && (interruptNum != UNUSED_PICINT))
  {
	  uint16_t pciCtrlNum = (interruptNum / PIN_CHANGE_NUM_INTERRUPTS);	// There are only 3 separate pin change interrupt vectors
	  uint16_t pciMaskBit = (interruptNum % 8);							// The interrupt number corresponds to a bit of the PCMSKx register

	  noInterrupts(); // Start critical section (I'm messing with the interrupts!)
	  switch(pciCtrlNum){
	  case PCI0:
		  PCMSK0 &= ~(1 << pciMaskBit);
		  break;
	  case PCI1:
		  PCMSK1 &= ~(1 << pciMaskBit);
		  break;
	  case PCI2:
		  PCMSK2 &= ~(1 << pciMaskBit);
		  break;
	  }
	  PCICR &= ~(1 << pciCtrlNum);
	  interrupts(); // End critical section (I'm done messing with the interrupts)
  }
}

SIGNAL(PCINT0_vect) {
  if(PCMSK0 != 0)
    intFunc[PCI0]();
}

SIGNAL(PCINT1_vect) {
  if(PCMSK1 != 0)
    intFunc[PCI1]();
}

SIGNAL(PCINT2_vect) {
  if(PCMSK2 != 0)
    intFunc[PCI2]();
}

// Pass the pin change interrupt number and the pin associated with that pin change interrupt will be returned
uint16_t InterruptConstants::GetPinChangeInterruptPin(const PinChangeInterruptNumber_t interNum)
{
	uint16_t returnVal = 999;
	switch (interNum)
	{
	case PCINT_0:
		returnVal = 53;
		break;
	case PCINT_1:
		returnVal = 52;
		break;
	case PCINT_2:
		returnVal = 51;
		break;
	case PCINT_3:
		returnVal = 50;
		break;
	case PCINT_4:
		returnVal = 10;
		break;
	case PCINT_5:
		returnVal = 11;
		break;
	case PCINT_6:
		returnVal = 12;
		break;
	case PCINT_7:
		returnVal = 13;
		break;
	case PCINT_8:
		returnVal = 0;
		break;
	case PCINT_9:
		returnVal = 15;
		break;
	case PCINT_10:
		returnVal = 14;
		break;
	case PCINT_16:
		returnVal = A8;
		break;
	case PCINT_17:
		returnVal = A9;
		break;
	case PCINT_18:
		returnVal = A10;
		break;
	case PCINT_19:
		returnVal = A11;
		break;
	case PCINT_20:
		returnVal = A12;
		break;
	case PCINT_21:
		returnVal = A13;
		break;
	case PCINT_22:
		returnVal = A14;
		break;
	case PCINT_23:
		returnVal = A15;
		break;
	case UNUSED_PICINT:
	default:
		returnVal = 999;
		break;
	}
	return (returnVal);
}
#endif
