// Author: 		Luke Spicer
// Date:		July 11th, 2012
// Purpose:		Type Definitions for Interrupts
// Description:	Allow classes and functions to force only the enumerated
//				values for the interrupt number and interrupt mode be used

#ifndef PCINT_types_h
#define PCINT_types_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

namespace InterruptConstants {

// Define an enumerated type for the various possible external interrupts
typedef enum
{
	// These interrupts are defined for all Arduino variants
	Interrupt_Zero =	0,		// on digital pin 2
	Interrupt_One = 	1,		// on digital pin 3
#if defined(__AVR_ATmega1284P__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
	// These interrupts are only defined for Arduino Mega variants
	Interrupt_Two = 	2,		// on digital pin 21
	Interrupt_Three = 	3,		// on digital pin 20
	Interrupt_Four = 	4,		// on digital pin 19
	Interrupt_Five = 	5,		// on digital pin 18
#endif
} InterruptNumber_t;

// Pass the interrupt number and the pin associated with that interrupt will be returned
uint16_t GetInterruptPin(const InterruptNumber_t interNum);

// Define an enumerated type for the various external interrupt triggering modes
typedef enum
{
	// The interrupt modes are defined by #defines, but these do not enforce the limited set of values
	Interrupt_Low = 	LOW,		// LOW to trigger the interrupt whenever the pin is low,
	Interrupt_Change = 	CHANGE,		// CHANGE to trigger the interrupt whenever the pin changes value
	Interrupt_Rising = 	RISING,		// RISING to trigger when the pin goes from low to high
	Interrupt_Falling = FALLING,	// FALLING for when the pin goes from high to low
} InterruptMode_t;

// Pin change interrupts only available to Arduino Mega variants currently
#if defined(__AVR_ATmega1284P__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
// Define an enumerated type for the various possible pin change interrupts
typedef enum
{
	UNUSED_PICINT =	777,	// Indicates an unused PCINT
	// PCI0
	PCINT_0 = 		0,		// on digital pin 53
	PCINT_1 = 		1,		// on digital pin 52
	PCINT_2 = 		2,		// on digital pin 51
	PCINT_3 = 		3,		// on digital pin 50
	PCINT_4 = 		4,		// on digital pin 10
	PCINT_5 = 		5,		// on digital pin 11
	PCINT_6 = 		6,		// on digital pin 12
	PCINT_7 = 		7,		// on digital pin 13

	// PCI1
	PCINT_8 = 		8,		// on digital pin 0
	PCINT_9 = 		9,		// on digital pin 15
	PCINT_10 = 		10,		// on digital pin 14
	PCINT_11 = 		UNUSED_PICINT,
	PCINT_12 = 		UNUSED_PICINT,
	PCINT_13 = 		UNUSED_PICINT,
	PCINT_14 = 		UNUSED_PICINT,
	PCINT_15 = 		UNUSED_PICINT,

	// PCI2
	PCINT_16 = 		16,		// on digital pin A8
	PCINT_17 = 		17,		// on digital pin A9
	PCINT_18 = 		18,		// on digital pin A10
	PCINT_19 = 		19,		// on digital pin A11
	PCINT_20 = 		20,		// on digital pin A12
	PCINT_21 = 		21,		// on digital pin A13
	PCINT_22 = 		22,		// on digital pin A14
	PCINT_23 = 		23,		// on digital pin A15
	NUM_PCINT = 	24,		// Indicates the number of PCINT pins
} PinChangeInterruptNumber_t;

///
// AttachPinChangeInterrupt
// Return: 		void
// Parameters:	pin change interrupt number (pin change interrupt numbers defined in PinChangeInterruptNumber_t)
//				function pointer to the pin change interrupt handler
// Description: Turns on the selected pin change interrupt and registers the user function
// Result:		Any value change on the pin corresponding to the interrupt number will trigger the registered function
void AttachPinChangeInterrupt(PinChangeInterruptNumber_t interruptNum, void (*userFunc)(void));

///
// DetachPinChangeInterrupt
// Return: 		void
// Parameters:	pin change interrupt number to disable (pin change interrupt numbers defined in PinChangeInterruptNumber_t)
// Description: Turns off the selected pin change interrupt
// Result:		The pin corresponding to the interrupt number will no longer trigger pin change interrupts
void DetachPinChangeInterrupt(PinChangeInterruptNumber_t interruptNum);

// Pass the pin change interrupt number and the pin associated with that pin change interrupt will be returned
uint16_t GetPinChangeInterruptPin(const PinChangeInterruptNumber_t interNum);
#endif
};

#endif
